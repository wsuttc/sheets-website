/**
 * isAuthorized
 *
 * @description :: Policy to check if user is authorized
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Policies
 */

module.exports = async function(req, res, next) {
  let token;
  if (req.headers && req.headers.authorization) {
    let parts = req.headers.authorization.split(" ");
    if (parts.length === 2) {
      let scheme = parts[0];
      let credentials = parts[1];

      if (/^Bearer$/i.test(scheme)) {
        token = credentials;
      }
    } else {
      return res.status(401).json({
        error:
          "Error with authorization. Format is Authorization: Bearer [token]"
      });
    }
  } else if (req.param("token")) {
    token = req.param("token");
    // We delete the token from param to not mess with blueprints
    delete req.query.token;
  } else {
    return res
      .status(401)
      .json({ error: "This endpoint requires authorization." });
  }

  if (token !== sails.config.custom.discordBot.token) {
    return res
      .status(401)
      .json({ error: "The provided token is incorrect." });
  }

  return next();
};
