/**
 * Septemcharacters.js
 *
 * @description :: A collection of role play characters.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "septem",
  attributes: {
    userID: {
      type: "string",
      allowNull: true,
      description: "ID of the user who owns this character"
    },

    active: {
      type: "boolean",
      defaultsTo: false,
      description: "If false, the character will not show up on any web panels."
    },

    name: {
      type: "string",
      maxLength: 64,
      description: "Name of the character",
      required: true
    },

    image: {
      type: "string",
      allowNull: true,
      description: "image URL of the character if an image was provided."
    },

    isPrivate: {
      type: "boolean",
      defaultsTo: true,
      description:
        "Is this character a private character (some stats hidden from view of other panels)?"
    },

    tag: {
      type: "string",
      isIn: [
        "Mini Boss",
        "Final Boss",
        "Oversized Boss",
        "NPC",
        "Object",
        "Unknown Motive",
        "Clone"
      ],
      allowNull: true,
      description: "A tag if a ribbon should be visible for this character."
    },

    hasInspiration: {
      type: "boolean",
      defaultsTo: false,
      description: "Does this character have secret inspiration?"
    },

    class: {
      type: "string",
      maxLength: 255,
      allowNull: true,
      description: "Descriptor of the character class, if applicable."
    },

    gender: {
      type: "string",
      maxLength: 255,
      allowNull: true,
      description: "Gender of the character, if applicable."
    },

    age: {
      type: "string",
      maxLength: 255,
      allowNull: true,
      description: "A number or descriptor of this character's age."
    },

    height: {
      type: "number",
      required: true,
      description: "Height of the character in centimeters"
    },

    weight: {
      type: "number",
      required: true,
      description: "Weight of the character in kilograms"
    },

    appearance: {
      type: "string",
      maxLength: 65536,
      allowNull: true,
      description: "Description of the character appearance and physique"
    },

    personality: {
      type: "string",
      maxLength: 65536,
      allowNull: true,
      description: "Character personality"
    },

    biography: {
      type: "string",
      maxLength: 65536,
      allowNull: true,
      description: "Character biography and back story"
    },

    gmNotes: {
      type: "string",
      maxLength: 65536,
      allowNull: true,
      description: "Notes that should only be visible to GMs."
    },

    strengths: {
      type: "string",
      maxLength: 65536,
      allowNull: true,
      description:
        "Character strengths. Should contain dice modifiers where applicable."
    },

    weaknesses: {
      type: "string",
      maxLength: 65536,
      allowNull: true,
      description:
        "Character weaknesses. Should contain dice modifiers where applicable."
    },

    strength: {
      type: "number",
      min: 0,
      max: 30,
      defaultsTo: 1,
      description:
        "Determines + modifier on anything requiring physical strength, such as pushing, pulling, punching / physical combat, carrying, etc."
    },

    dexterity: {
      type: "number",
      min: 0,
      max: 30,
      defaultsTo: 1,
      description:
        "Determines + modifier on a characters ability to dodge an attack, or the distance they can move on a map in a turn."
    },

    constitution: {
      type: "number",
      min: 0,
      max: 30,
      defaultsTo: 1,
      description:
        "Determines + modifier on a characters ability to avert bad ailments / spells and apply powerups. Also determines max HP."
    },

    intelligence: {
      type: "number",
      min: 0,
      max: 30,
      defaultsTo: 1,
      description:
        "Determines + modifier on a characters ability to acquire information or clues."
    },

    wisdom: {
      type: "number",
      min: 0,
      max: 30,
      defaultsTo: 1,
      description:
        "Determines + modifier on a character’s ability to decipher what is going on around them (such as if they detect danger) or to avoid doing something stupid."
    },

    charisma: {
      type: "number",
      min: 0,
      max: 30,
      defaultsTo: 1,
      description:
        "Determines + modifier on a character’s ability to convince other characters to do something or to be-friend them."
    },

    armor: {
      type: "number",
      min: 0,
      max: 20,
      defaultsTo: 8,
      description:
        "Armor class (number required to hit when opponents roll to attack)"
    },

    movement: {
      type: "number",
      min: 0,
      defaultsTo: 25,
      description: "How many meters the character can move at max in one turn"
    },

    earthHP: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description: "Health in Earth form."
    },

    matrixHP: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description: "Health in Matrix form."
    },

    maxHP: {
      type: "number",
      min: 0,
      allowNull: true,
      description:
        "Max HP for this character. Null = determined by level and constitution."
    },

    earthEP: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description: "Energy in Earth form."
    },

    matrixEP: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description: "Energy in Matrix form."
    },

    maxEP: {
      type: "number",
      min: 0,
      allowNull: true,
      description: "Max EP for this character. Null = determined by level."
    },

    XP: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description:
        "Experience points, which determines the level of the character."
    },

    SRD: {
      type: "number",
      defaultsTo: 0,
      description: "Septem Regiones Dollars; currency in the game."
    },

    SP: {
      type: "number",
      min: 0,
      defaultsTo: 25,
      description:
        "Source code points, which requiem is trying to steal away from the character throughout the campaign to grow stronger."
    }
  },

  // Websockets
  afterCreate: function(data, proceed) {
    sails.sockets.broadcast(
      [`septem-character-${data.id}`, `septem-characters`],
      "septem-character-refresh",
      data.id
    );
    return proceed();
  },

  afterUpdate: function(data, proceed) {
    sails.sockets.broadcast(
      [`septem-character-${data.id}`, `septem-characters`],
      "septem-character-refresh",
      data.id
    );
    return proceed();
  },

  afterDestroy: function(data, proceed) {
    // Also remove all character buffs and possessed items as clean-up.
    (async _data => {
      await sails.models.septemcharacterbuffs
        .destroy({ character: _data.id })
        .fetch();

      await sails.models.septemitempossession
        .destroy({ character: _data.id })
        .fetch();
    })(data);

    sails.sockets.broadcast(
      [`septem-character-${data.id}`, `septem-characters`],
      "septem-character-remove",
      data.id
    );

    sails.sockets.leaveAll(`septem-character-${data.id}`);

    return proceed();
  }
};
