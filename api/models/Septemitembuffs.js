/**
 * Septemitembuffs.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "septem",
  attributes: {
    possession: {
      type: "number",
      required: true,
      description: "The septemitempossession id."
    },
    name: {
      type: "string",
      required: true,
      maxLength: 256,
      description:
        "Some descriptive name for the buff so we know where it is, what it is, and what caused it"
    },
    duration: {
      type: "number",
      allowNull: true,
      description:
        "Amount of time left (in minutes) this buff remains. Null = indefinite until removed by some means such as an item."
    },
    level: {
      type: "number",
      allowNull: true,
      description:
        "A number descriptor to describe the strength of this buff (such as amount of HP/EP, tier/level, etc)."
    },

    strength: {
      type: "number",
      allowNull: true,
      description:
        "If this buff modifies the amount of strength the item originally modifies in the possessing character, this is by how much (null = no modification)."
    },
    dexterity: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the amount of dexterity the item originally modifies in the possessing character, this is by how much (null = no modification)."
    },
    constitution: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the amount of constitution the item originally modifies in the possessing character, this is by how much (null = no modification)."
    },
    intelligence: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the amount of intelligence the item originally modifies in the possessing character, this is by how much (null = no modification)."
    },
    wisdom: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the amount of wisdom the item originally modifies in the possessing character, this is by how much (null = no modification)."
    },
    charisma: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the amount of charisma the item originally modifies in the possessing character, this is by how much (null = no modification)."
    },
    armor: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the armor class the item originally modifies in the possessing character, this is by how much (null = no modification)."
    },
    movement: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the movement the item originally modifies in the possessing character, this is by how much (null = no modification)."
    },
    HP: {
      type: "number",
      allowNull: true,
      description:
        "If this item's HP should be modified per minute on possessions, this is by how much (null = no modification)."
    },
    EP: {
      type: "number",
      allowNull: true,
      description:
        "If this item's EP should be modified per minute on possessions, this is by how much (null = no modification)."
    },
    maxHP: {
      type: "number",
      allowNull: true,
      description:
        "If this item's maxHP should be modified, this is by how much (null = no modification)."
    },
    maxEP: {
      type: "number",
      allowNull: true,
      description:
        "If this item's maxEP should be modified, this is by how much (null = no modification)."
    }
  },

  // Websockets
  afterCreate: function(data, proceed) {
    sails.sockets.broadcast(
      [`septem-itempossession-${data.possession}`],
      "septem-itempossession-refresh",
      data.possession
    );

    (async _data => {
      let possession = await sails.models.septemitempossession.findOne({
        id: _data.possession
      });
      if (possession) {
        sails.sockets.broadcast(
          [`septem-item-${possession.item}`],
          "septem-item-refresh",
          possession.item
        );

        if (possession.character) {
          sails.sockets.broadcast(
            [`septem-character-${possession.character}`],
            "septem-character-refresh",
            possession.character
          );
        }

        sails.sockets.broadcast(
          [`septem-characters`],
          "septem-character-refresh",
          0
        );
      }
    })(data);

    return proceed();
  },

  afterUpdate: function(data, proceed) {
    sails.sockets.broadcast(
      [`septem-itempossession-${data.possession}`],
      "septem-itempossession-refresh",
      data.possession
    );

    (async _data => {
      let possession = await sails.models.septemitempossession.findOne({
        id: _data.possession
      });
      if (possession) {
        sails.sockets.broadcast(
          [`septem-item-${possession.item}`],
          "septem-item-refresh",
          possession.item
        );

        if (possession.character) {
          sails.sockets.broadcast(
            [`septem-character-${possession.character}`],
            "septem-character-refresh",
            possession.character
          );
        }

        sails.sockets.broadcast(
          [`septem-characters`],
          "septem-character-refresh",
          0
        );
      }
    })(data);

    return proceed();
  },

  afterDestroy: function(data, proceed) {
    sails.sockets.broadcast(
      [`septem-itempossession-${data.possession}`],
      "septem-itempossession-refresh",
      data.possession
    );

    (async _data => {
      let possession = await sails.models.septemitempossession.findOne({
        id: _data.possession
      });
      if (possession) {
        sails.sockets.broadcast(
          [`septem-item-${possession.item}`],
          "septem-item-refresh",
          possession.item
        );

        if (possession.character) {
          sails.sockets.broadcast(
            [`septem-character-${possession.character}`],
            "septem-character-refresh",
            possession.character
          );
        }

        sails.sockets.broadcast(
          [`septem-characters`],
          "septem-character-refresh",
          0
        );
      }
    })(data);

    return proceed();
  }
};
