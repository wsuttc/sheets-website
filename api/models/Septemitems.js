/**
 * Items.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "septem",
  attributes: {
    name: {
      type: "string",
      maxLength: 256,
      description: "Name of the item",
      required: true
    },

    image: {
      type: "string",
      allowNull: true,
      description: "image URL of the item if an image was provided."
    },

    type: {
      type: "string",
      isIn: [
        "Weapon",
        "Ability",
        "Spell",
        "Feature",
        "Artifact",
        "Armor",
        "Food",
        "Drink",
        "Vehicle",
        "Equipment",
        "Medicine",
        "Software",
        "Hardware"
      ],
      required: true,
      description: "Type of item"
    },

    actionLink: {
      type: "string",
      allowNull: true,
      description:
        "I a button should be displayed on the GM panel that executes a controller on click. Path should be relative to serverURL/septem/. Will pass character id as character parameter."
    },

    weight: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description:
        "Weight of the item, in kg, if applicable and item should count towards an item-carrying weight limit."
    },

    info: {
      type: "string",
      maxLength: 65536,
      allowNull: true,
      description:
        "Background information about this item including what it can do."
    },

    HP: {
      type: "number",
      min: 0,
      allowNull: true,
      description:
        "The maximum / starting HP for this item. Use null if this item does not use HP mechanics."
    },

    loseHPPerMinute: {
      type: "number",
      min: 0,
      allowNull: true,
      description:
        "If this item should lose HP over time (such as spoiling), specify how much HP per hour it loses (decimals permitted)."
    },

    EP: {
      type: "number",
      min: 0,
      allowNull: true,
      description:
        "The maximum / starting EP for this item. Use null if this item does not use EP."
    },

    charges: {
      type: "number",
      min: 0,
      allowNull: true,
      description:
        "The number of charges this item has when assigned. Use null if this item does not use charges."
    },

    levelsPerCharge: {
      type: "number",
      min: 0,
      allowNull: true,
      description:
        "If this item accumulates charges based on level, specify the number of levels a character must advance to earn one charge of this item. If you use a number below one, that is multiple charges per level (eg. 0.5 = 2 charges per level)."
    },

    price: {
      type: "number",
      min: 0,
      allowNull: true,
      description:
        "The cost to purchase this item (in SRD). Use null if not applicable."
    },

    strength: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the possessing character's strength, this is by how much (null = no modification)."
    },
    dexterity: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the possessing character's dexterity, this is by how much (null = no modification)."
    },
    constitution: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the possessing character's constitution, this is by how much (null = no modification)."
    },
    intelligence: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the possessing character's intelligence, this is by how much (null = no modification)."
    },
    wisdom: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the possessing character's wisdom, this is by how much (null = no modification)."
    },
    charisma: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the possessing character's charisma, this is by how much (null = no modification)."
    },
    armor: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the possessing character's armor class, this is by how much (null = no modification)."
    },
    movement: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the possessing character's movement, this is by how much (null = no modification)."
    },
    maxHP: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the character's maxHP, this is by how much (null = no modification)."
    },
    maxEP: {
      type: "number",
      allowNull: true,
      description:
        "If this item modifies the character's maxEP, this is by how much (null = no modification)."
    }
  },

  // Websockets
  afterCreate: function(data, proceed) {
    sails.sockets.broadcast(
      [`septem-item-${data.id}`],
      "septem-item-refresh",
      data.id
    );

    sails.sockets.broadcast(
      [`septem-characters`],
      "septem-character-refresh",
      0
    );

    // Also update individual characters who possess this item
    (async _data => {
      let possessions = await sails.models.septemitempossession.find({
        item: _data.id
      });
      possessions
        .filter(possession => possession.character)
        .map(possession => {
          sails.sockets.broadcast(
            [`septem-character-${possession.character}`],
            "septem-character-refresh",
            possession.character
          );
        });
    })(data);

    return proceed();
  },

  afterUpdate: function(data, proceed) {
    sails.sockets.broadcast(
      [`septem-item-${data.id}`],
      "septem-item-refresh",
      data.id
    );

    sails.sockets.broadcast(
      [`septem-characters`],
      "septem-character-refresh",
      0
    );

    // Also update individual characters who possess this item
    (async _data => {
      let possessions = await sails.models.septemitempossession.find({
        item: _data.id
      });
      possessions
        .filter(possession => possession.character)
        .map(possession => {
          sails.sockets.broadcast(
            [`septem-character-${possession.character}`],
            "septem-character-refresh",
            possession.character
          );
        });
    })(data);

    return proceed();
  },

  afterDestroy: function(data, proceed) {
    // Also remove all item possessions as clean-up
    (async _data => {
      await sails.models.septemitempossession
        .destroy({ item: _data.id })
        .fetch();
    })(data);

    sails.sockets.broadcast(
      [`septem-item-${data.id}`],
      "septem-item-remove",
      data.id
    );

    sails.sockets.broadcast(
      [`septem-characters`],
      "septem-character-refresh",
      0
    );

    sails.sockets.leaveAll(`septem-item-${data.id}`);

    return proceed();
  }
};
