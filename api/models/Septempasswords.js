/**
 * Septempasswords.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "septem",
  attributes: {
    name: {
      required: true,
      unique: true,
      type: "string",
      description: "Codename for this password"
    },
    value: {
      required: true,
      type: "string",
      description: "The password"
    }
  }
};
