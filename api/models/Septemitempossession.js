/**
 * Septempossession.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "septem",
  attributes: {
    item: {
      type: "number",
      required: true,
      description: "The Septemitem id."
    },
    character: {
      type: "number",
      allowNull: true,
      description:
        "The Septemcharacter id who owns this item. Null = item is not currently owned by anyone."
    },
    possessed: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, the item is currently on the character's body and subject to relevant buffs and modifiers."
    },
    location: {
      type: "string",
      allowNull: true,
      description: "Where the item is currently located."
    },
    HP: {
      type: "number",
      min: 0,
      allowNull: true,
      description: "HP left for this item (Null = not applicable)."
    },
    EP: {
      type: "number",
      min: 0,
      allowNull: true,
      description: "Energy points left for this item (Null = not applicable)."
    },
    XP: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description: "XP for this item."
    },
    charges: {
      type: "number",
      min: 0,
      allowNull: true,
      description:
        "The number of charges this item has left (Null = not applicable)."
    }
  },

  // Websockets
  afterCreate: function(data, proceed) {
    sails.sockets.broadcast(
      [`septem-itempossession-${data.id}`],
      "septem-itempossession-refresh",
      data.id
    );

    sails.sockets.broadcast(
      [`septem-item-${data.item}`],
      "septem-item-refresh",
      data.item
    );

    if (data.character) {
      sails.sockets.broadcast(
        [`septem-character-${data.character}`],
        "septem-character-refresh",
        data.character
      );
    }

    sails.sockets.broadcast(
      [`septem-characters`],
      "septem-character-refresh",
      0
    );

    return proceed();
  },

  afterUpdate: function(data, proceed) {
    sails.sockets.broadcast(
      [`septem-itempossession-${data.id}`],
      "septem-itempossession-refresh",
      data.id
    );

    sails.sockets.broadcast(
      [`septem-item-${data.item}`],
      "septem-item-refresh",
      data.item
    );

    if (data.character) {
      sails.sockets.broadcast(
        [`septem-character-${data.character}`],
        "septem-character-refresh",
        data.character
      );
    }

    sails.sockets.broadcast(
      [`septem-characters`],
      "septem-character-refresh",
      0
    );

    return proceed();
  },

  afterDestroy: function(data, proceed) {
    // Also remove all item buffs as clean-up
    (async _data => {

      await sails.models.septemitembuffs
        .destroy({ possession: _data.id })
        .fetch();

    })(data);

    sails.sockets.broadcast(
      [`septem-itempossession-${data.id}`],
      "septem-itempossession-remove",
      data.id
    );

    sails.sockets.broadcast(
      [`septem-item-${data.item}`],
      "septem-item-refresh",
      data.item
    );

    if (data.character) {
      sails.sockets.broadcast(
        [`septem-character-${data.character}`],
        "septem-character-refresh",
        data.character
      );
    }

    sails.sockets.broadcast(
      [`septem-characters`],
      "septem-character-refresh",
      0
    );

    sails.sockets.leaveAll(`septem-itempossession-${data.id}`);

    return proceed();
  }
};
