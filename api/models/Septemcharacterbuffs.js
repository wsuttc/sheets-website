/**
 * Septemcharacterbuffs.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  datastore: "septem",
  attributes: {
    character: {
      type: "number",
      required: true,
      description: "The Septemcharacter id."
    },
    item: {
      type: "number",
      allowNull: true,
      description:
        "If triggered by an item, this is the Septemitem that applied the buff."
    },
    name: {
      type: "string",
      required: true,
      maxLength: 256,
      description:
        "Some descriptive name for the buff so we know where it is, what it is, and what caused it"
    },
    duration: {
      type: "number",
      allowNull: true,
      description:
        "Amount of time left (in minutes) this buff remains. Null = indefinite until removed by some means such as an item."
    },
    level: {
      type: "number",
      allowNull: true,
      description:
        "A number descriptor to describe the strength of this buff (such as amount of HP/EP, tier/level, etc)."
    },
    strength: {
      type: "number",
      allowNull: true,
      description:
        "If this buff modifies the character's strength, this is by how much (null = no modification)."
    },
    dexterity: {
      type: "number",
      allowNull: true,
      description:
        "If this buff modifies the character's dexterity, this is by how much (null = no modification)."
    },
    constitution: {
      type: "number",
      allowNull: true,
      description:
        "If this buff modifies the character's constitution, this is by how much (null = no modification)."
    },
    intelligence: {
      type: "number",
      allowNull: true,
      description:
        "If this buff modifies the character's intelligence, this is by how much (null = no modification)."
    },
    wisdom: {
      type: "number",
      allowNull: true,
      description:
        "If this buff modifies the character's wisdom, this is by how much (null = no modification)."
    },
    charisma: {
      type: "number",
      allowNull: true,
      description:
        "If this buff modifies the character's charisma, this is by how much (null = no modification)."
    },
    armor: {
      type: "number",
      allowNull: true,
      description:
        "If this buff modifies the character's armor class, this is by how much (null = no modification)."
    },
    movement: {
      type: "number",
      allowNull: true,
      description:
      "If this buff modifies the character's max movement, this is by how much (null = no modification)."
    },
    earthHP: {
      type: "number",
      allowNull: true,
      description:
      "If this buff modifies the character's earthHP, this is by how much per minute (null = no modification)."
    },
    earthEP: {
      type: "number",
      allowNull: true,
      description:
      "If this buff modifies the character's earthEP, this is by how much per minute (null = no modification)."
    },
    matrixHP: {
      type: "number",
      allowNull: true,
      description:
      "If this buff modifies the character's matrixHP, this is by how much per minute (null = no modification)."
    },
    matrixEP: {
      type: "number",
      allowNull: true,
      description:
      "If this buff modifies the character's matrixEP, this is by how much per minute (null = no modification)."
    },
    maxHP: {
      type: "number",
      allowNull: true,
      description:
      "If this buff modifies the character's maxHP, this is by how much (null = no modification)."
    },
    maxEP: {
      type: "number",
      allowNull: true,
      description:
      "If this buff modifies the character's maxEP, this is by how much (null = no modification)."
    },
    expireCharacter: {
      type: "boolean",
      defaultsTo: false,
      description: "If true, the associated character will be permanently deleted (as if temporary) when the buff expires."
    },
  },

  // Websockets
  afterCreate: function(data, proceed) {
    sails.sockets.broadcast(
      [`septem-character-${data.character}`, `septem-characters`],
      "septem-character-refresh",
      data.character
    );
    return proceed();
  },

  afterUpdate: function(data, proceed) {
    sails.sockets.broadcast(
      [`septem-character-${data.character}`, `septem-characters`],
      "septem-character-refresh",
      data.character
    );
    return proceed();
  },

  afterDestroy: function(data, proceed) {
    sails.sockets.broadcast(
      [`septem-character-${data.character}`, `septem-characters`],
      "septem-character-refresh",
      data.character
    );
    return proceed();
  }
};
