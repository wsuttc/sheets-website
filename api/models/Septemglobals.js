/**
 * Septemglobals.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */
const moment = require("moment");

module.exports = {
  datastore: "septem",
  attributes: {
    time: {
      type: "number",
      defaultsTo: moment().unix(),
      description: "The current unix timestamp of the Septem campaign."
    },
    requiemAttackLevel: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description:
        "Current Requiem attack level. requiem CP regeneration is 1 CP per level per minute, multiplied by redCCUs. An attack level cannot go above (requiemSP / 25) + 1."
    },
    requiemSP: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description: "Number of source points Requiem currently possesses."
    },
    requiemCP: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description:
        "Number of code points for use by Requiem (the antagonists). Re-generation is requiemAttackLevel * redCCUs per minute."
    },
    computaCP: {
      type: "number",
      min: 0,
      defaultsTo: 100,
      description:
        "Number of code points for use by the supercomputer operator. Re-generation is 2 CP per minute per greenCCUs."
    },

    redCCUs: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description: "Number of CCUs currently under Requiem control (red)."
    },
    orangeCCUs: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description:
        "Number of CCUs currently in orange status (maintenance required)."
    },
    yellowCCUs: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description: "Number of CCUs currently offering an upgrade (yellow)."
    },
    greenCCUs: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description:
        "Number of CCUs currently under control of whomever is operating the supercomputer (green)."
    },
    indigoCCUs: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description:
        "Number of CCUs active with a communication channel to other universes etc (indigo)."
    },
    violetCCUs: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description:
        "Number of CCUs with information / downloads available (violet)."
    },
    whiteCCUs: {
      type: "number",
      min: 0,
      defaultsTo: 0,
      description:
        "Number of CCUs under control of Lovinity or the person who created the Matrix."
    },
    totalCCUs: {
      type: "number",
      min: 0,
      defaultsTo: 57,
      description:
        "Total number of CCUs that exist in the Matrix."
    }
  },

  // Websockets
  afterCreate: function(data, proceed) {
    sails.sockets.broadcast(
      [`septem-globals`],
      "septem-globals-refresh",
      data.id
    );
    return proceed();
  },

  afterUpdate: function(data, proceed) {
    sails.sockets.broadcast(
      [`septem-globals`],
      "septem-globals-refresh",
      data.id
    );
    return proceed();
  }
};
