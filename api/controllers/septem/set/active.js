module.exports = {
  friendlyName: "septem/set/active",

  description: "Set the active state of one or more characters.",

  inputs: {
    active: {
      type: "boolean",
      required: true,
      description: "Mark the character(s) as active or inactive."
    },
    ids: {
      type: "json",
      required: true,
      custom: value => value.constructor === Array && value.length > 0,
      description: "The character ids to change active status."
    }
  },

  fn: async function(inputs) {
    let maps = inputs.ids.map(async id => {
      await sails.models.septemcharacters.updateOne(
        { id: id },
        { active: inputs.active }
      );
    });
    await Promise.all(maps);

    return { success: true };
  }
};
