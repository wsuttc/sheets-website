const moment = require("moment");

module.exports = {
  friendlyName: "septem/set/time",

  description: "Adjust the campaign time",

  inputs: {
    time: {
      type: "number",
      required: true,
      min: 1,
      description: "Amount of time to add in seconds"
    }
  },

  fn: async function(inputs) {
    const earthHPRegeneration = 0.1; // Amount of HP re-generated per minute for earthHP.

    // Get current time
    let globalValues = await sails.models.septemglobals.findOne({ id: 1 });
    if (!globalValues)
      return { error: "Could not find the current campaign time." };

    let info = [];
    let maps;
    let maps2;

    // Calculate new unix time stamp
    let newTime = moment
      .unix(globalValues.time)
      .add(inputs.time, "seconds")
      .unix();

    // Calculate how many floating minutes we advanced
    let difference = moment
      .unix(newTime)
      .diff(moment.unix(globalValues.time), "minutes", true);

    // Re-generate CP; update campaign time
    let criteria2 = {
      time: newTime,
      requiemCP: (
        globalValues.requiemAttackLevel * globalValues.redCCUs * difference +
        globalValues.requiemCP
      ).toFixed(2),
      computaCP: (
        globalValues.greenCCUs * 2 * difference +
        globalValues.computaCP
      ).toFixed(2)
    };
    await sails.models.septemglobals.updateOne({ id: 1 }, criteria2);

    // Get active character buffs
    let characterBuffs = await sails.models.septemcharacterbuffs.find();

    // Do stuff depending on the buffs
    if (characterBuffs.length > 0) {
      maps = characterBuffs.map(async buff => {
        // Get the character the buff applies to
        let character = await sails.models.septemcharacters.findOne({
          id: buff.character
        });

        // Calculate the new remainder for the buff and how long we ran for
        let newDuration =
          buff.duration !== null
            ? (buff.duration - difference).toFixed(2)
            : null;
        let ranFor = 0;

        // Build a string so the GM knows what buffs are active
        let buffString = `<strong>CHAR BUFF</strong>: ${buff.name} (${
          buff.id
        }) applied to ${
          character
            ? `${character.name} (${character.id})`
            : `Unknown Character`
        }`;

        if (newDuration !== null && newDuration <= 0) {
          buffString += ` <strong>EXPIRED</strong> (ran for ${buff.duration} minutes).`;
          ranFor = buff.duration;

          // Erase already-expired buffs and do not make a mention of it.
          await sails.models.septemcharacterbuffs.destroyOne({ id: buff.id });

          // Also erase character if buff has expireCharacter
          if (buff.expireCharacter) {
            buffString += ` <strong>Character Expired!</strong>`;
            await sails.models.septemcharacters.destroyOne({
              id: buff.character
            });
          }
        } else {
          buffString += ` ran for ${difference} minutes${
            newDuration !== null ? ` (${newDuration} minutes remaining)` : ""
          }`;
          ranFor = difference;
        }

        // Adjust HP/EP if the buff does so
        let stats = await sails.helpers.septem.stats(buff.character);

        if (stats) {
          let criteria = {};
          if (buff.earthHP && character.earthHP < stats.currentStats.maxHP)
            criteria.earthHP = (
              buff.earthHP * ranFor +
              character.earthHP
            ).toFixed(2);
          if (buff.matrixHP && character.matrixHP < stats.currentStats.maxHP)
            criteria.matrixHP = (
              buff.matrixHP * ranFor +
              character.matrixHP
            ).toFixed(2);
          if (buff.earthEP && character.earthEP < stats.currentStats.maxEP)
            criteria.earthEP = (
              buff.earthEP * ranFor +
              character.earthEP
            ).toFixed(2);
          if (buff.matrixEP && character.matrixEP < stats.currentStats.maxEP)
            criteria.matrixEP = (
              buff.matrixEP * ranFor +
              character.matrixEP
            ).toFixed(2);

          // Overload checks
          if (criteria.earthHP && criteria.earthHP > stats.currentStats.maxHP)
            criteria.earthHP = stats.currentStats.maxHP;
          if (criteria.matrixHP && criteria.matrixHP > stats.currentStats.maxHP)
            criteria.matrixHP = stats.currentStats.maxHP;
          if (criteria.earthEP && criteria.earthEP > stats.currentStats.maxEP)
            criteria.earthEP = stats.currentStats.maxEP;
          if (criteria.matrixEP && criteria.matrixEP > stats.currentStats.maxEP)
            criteria.matrixEP = stats.currentStats.maxEP;

          await sails.models.septemcharacters.updateOne(
            { id: character.id },
            criteria
          );
        }

        // Update duration
        await sails.models.septemcharacterbuffs.updateOne(
          { id: buff.id },
          { duration: newDuration }
        );

        info.push(buffString);
      });
      await Promise.all(maps);
    }

    // Get active item buffs
    let itemBuffs = await sails.models.septemitembuffs.find();

    // Do stuff depending on the buffs
    if (itemBuffs.length > 0) {
      maps = itemBuffs.map(async buff => {
        // Get the possession the buff applies to
        let possession = await sails.models.septemitempossession.findOne({
          id: buff.possession
        });
        // Get the item
        let item = await sails.models.septemitems.findOne({
          id: possession.item
        });
        // Get the character who possesses the item
        let character = await sails.models.septemcharacters.findOne({
          id: possession.character
        });

        // Calculate the new remainder for the buff and how long we ran for
        let newDuration =
          buff.duration !== null
            ? (buff.duration - difference).toFixed(2)
            : null;
        let ranFor = 0;

        // Build a string so the GM knows what buffs are active
        let buffString = `<strong>ITEM BUFF</strong>: ${buff.name} (${
          buff.id
        }) applied to item ${
          item ? `${item.name} (${item.id})` : "Unknown"
        }, possession ${possession ? possession.id : `Unknown`}, character ${
          character
            ? `${character.name} (${character.id})`
            : `Unknown Character`
        }`;

        if (newDuration !== null && newDuration <= 0) {
          buffString += ` <strong>EXPIRED</strong> (ran for ${buff.duration} minutes).`;
          ranFor = buff.duration;

          // Erase already-expired buffs and do not make a mention of it.
          await sails.models.septemitembuffs.destroyOne({ id: buff.id });
        } else {
          buffString += ` ran for ${difference} minutes${
            newDuration !== null ? ` (${newDuration} minutes remaining)` : ""
          }`;
          ranFor = difference;
        }

        // Adjust HP/EP if the buff does so
        // TODO: use maxHP and maxEP checks on items
        let criteria = {};
        if (buff.HP && possession.HP) {
          criteria.HP = (buff.HP * ranFor + possession.HP).toFixed(2);
        }
        if (buff.EP && possession.EP) {
          criteria.EP = (buff.EP * ranFor + possession.EP).toFixed(2);
        }
        await sails.models.septemitempossession.updateOne(
          { id: possession.id },
          criteria
        );

        // Update duration
        await sails.models.septemitembuffs.updateOne(
          { id: buff.id },
          { duration: newDuration }
        );

        info.push(buffString);
      });
      await Promise.all(maps);
    }

    // Decrease HP for relevant items
    let hpKillers = await sails.models.septemitems.find({
      loseHPPerMinute: { ">": 0 }
    });
    if (hpKillers.length > 0) {
      maps = hpKillers.map(async item => {
        let possessions = await sails.models.septemitempossession.find({
          item: item.id
        });
        if (possessions.length > 0) {
          maps2 = possessions.map(async possession => {
            await sails.models.septemitempossession.updateOne(
              { id: possession.id },
              {
                HP: (
                  possession.HP - (item.loseHPPerMinute * difference).toFixed(2)
                ).toFixed(2)
              }
            );
          });
          await Promise.all(maps2);
        }
      });
      await Promise.all(maps);
    }

    // Delete item possessions which hit 0 HP
    let deadItems = await sails.models.septemitempossession.find({
      HP: { "<=": 0, "!=": null }
    });
    if (deadItems.length > 0) {
      maps = deadItems.map(async possession => {
        // Get the item
        let item = await sails.models.septemitems.findOne({
          id: possession.item
        });
        // Get the character who possesses the item
        let character = await sails.models.septemcharacters.findOne({
          id: possession.character
        });

        // Delete the item possession
        await sails.models.septemitempossession.destroyOne({
          id: possession.id
        });

        info.push(
          `<strong>ITEM DESTROYED</strong> (lost all HP): Item ${
            item ? `${item.name} (${item.id})` : "Unknown"
          }, possession ${possession ? possession.id : `Unknown`}, character ${
            character
              ? `${character.name} (${character.id})`
              : `Unknown Character`
          }`
        );
      });
      await Promise.all(maps);
    }

    // Re-generate earthHP, earthEP, and matrixEP
    let characters = await sails.models.septemcharacters.find({});
    if (characters.length > 0) {
      maps = characters.map(async character => {
        // Get current stats
        let stats = await sails.helpers.septem.stats(character.id);

        if (stats) {
          // Determine EP re-generation per minute
          let epRegen = stats.modifiers.dexterity + 5;

          let criteria = {
            earthHP:
              character.earthHP && character.earthHP < stats.currentStats.maxHP
                ? (
                    earthHPRegeneration * difference +
                    character.earthHP
                  ).toFixed(2)
                : character.earthHP,
            earthEP:
              character.earthEP && character.earthEP < stats.currentStats.maxEP
                ? (epRegen * difference + character.earthEP).toFixed(2)
                : character.earthEP,
            matrixEP:
              character.matrixEP &&
              character.matrixEP < stats.currentStats.maxEP
                ? (epRegen * difference + character.matrixEP).toFixed(2)
                : character.matrixEP
          };

          // Overload checks
          if (criteria.earthHP && criteria.earthHP > stats.currentStats.maxHP)
            criteria.earthHP = stats.currentStats.maxHP;
          if (criteria.earthEP && criteria.earthEP > stats.currentStats.maxEP)
            criteria.earthEP = stats.currentStats.maxEP;
          if (criteria.matrixEP && criteria.matrixEP > stats.currentStats.maxEP)
            criteria.matrixEP = stats.currentStats.maxEP;

          // Update values
          await sails.models.septemcharacters.updateOne(
            { id: character.id },
            criteria
          );
        }
      });
      await Promise.all(maps);
    }

    // Done; return important info
    return { info: info };
  }
};
