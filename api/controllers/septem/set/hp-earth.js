module.exports = {
  friendlyName: "septem/set/hp-earth",

  description: "Modify or set the HP of characters on Earth",

  inputs: {
    set: {
      type: "boolean",
      defaultsTo: false,
      description:
        "If true, we will explicitly set HP to amount instead of adjusting by it."
    },
    force: {
      type: "boolean",
      defaultsTo: false,
      description: "If true, we will allow HP to go over the maxHP."
    },
    amount: {
      type: "number",
      required: true,
      description: "Amount to adjust or set HP."
    },
    ids: {
      type: "json",
      required: true,
      custom: value => value.constructor === Array && value.length > 0,
      description: "The character ids to change HP."
    }
  },

  fn: async function(inputs) {
    let maps = inputs.ids.map(async id => {
      // Get current HP
      let character = await sails.models.septemcharacters.findOne({ id: id });
      let hp = character.earthHP;

      // Adjust HP
      if (inputs.set) {
        hp = inputs.amount;
      } else {
        hp += inputs.amount;
      }

      // Add checks
      if (hp < 0) hp = 0;
      if (hp > character.maxHP && !inputs.force) hp = character.maxHP;

      // Update with new value
      await sails.models.septemcharacters.updateOne(
        { id: id },
        { earthHP: hp }
      );
    });
    await Promise.all(maps);

    return { success: true };
  }
};
