module.exports = {
  friendlyName: "septem/ability/cloning",

  description:
    "Execute this endpoint when a character uses the cloning ability.",

  inputs: {
    character: {
      type: "number",
      required: true,
      description: "The character using the cloning ability."
    }
  },

  exits: {},

  fn: async function(inputs) {
    const costEP = 15; // The cost in EP to clone
    const cloneDuration = 3; // How many minutes a clone lasts

    // Find the character
    let character = await sails.models.septemcharacters.findOne({
      id: inputs.character
    });
    if (!character) return { error: "Character not found" };

    // Must have at least 10 matrix HP to clone
    if (character.matrixHP < 10)
      return { error: "Character must have at least 10 Matrix HP to clone." };

    // Must have at least costEP Matrix EP to clone
    if (character.matrixEP < costEP)
      return { error: "Character must have at least 15 Matrix EP to clone." };

    let stats = await sails.helpers.septem.stats(character.id);

    // Pre-populate a clone record
    let criteria = {
      active: true,
      name: character.name,
      image: character.image,
      isPrivate: character.isPrivate,
      tag: "Clone",
      hasInspiration: false,
      class: character.class,
      gender: character.gender,
      age: character.age,
      height: character.height,
      weight: character.weight,
      appearance: character.appearance,
      personality: character.personality,
      biography: "This is a clone of the original character by the same name.",
      strengths: character.strengths,
      weaknesses: character.weaknesses,
      strength: character.strength,
      dexterity: character.dexterity,
      constitution: character.constitution,
      intelligence: character.intelligence,
      wisdom: character.wisdom,
      charisma: character.charisma,
      armor: character.armor,
      movement: character.movement,
      earthHP: 0, // No Earth HP; this is a matrix-only ability
      matrixHP: Math.round(character.matrixHP / 4), // Clones only get 25% of host HP
      maxHP: Math.round(stats ? stats.baseStats.maxHP / 4 : 0),
      matrixEP: character.matrixEP - costEP, // Clones get host EP AFTER the cost EP has been docked
      maxEP: stats ? stats.baseStats.maxEP - costEP : 0, // Clone MaxEP is host maxEP minus costEP
      XP: character.XP,
      SRD: 0, // Clones do not need money
      SP: 0 // Clones do not have SP
    };

    // Subtract EP from main character
    await sails.models.septemcharacters.updateOne(
      { id: inputs.character },
      { matrixEP: character.matrixEP - costEP }
    );

    // Clone the character (but not their buffs)
    let clone = await sails.models.septemcharacters.create(criteria).fetch();

    // Add an expiration buff
    let cloneBuff = await sails.models.septemcharacterbuffs
      .create({
        character: clone.id,
        name: "Clone Expiration",
        duration: cloneDuration,
        expireCharacter: true
      })
      .fetch();

    // Clone abilities, spells, and features from the host if possessed (but not their buffs)
    let items = await sails.models.septemitems.find({
      type: ["Ability", "Spell", "Feature"]
    });
    items = items.map(i => i.id);
    let itemPossessions = await sails.models.septemitempossession.find({
      character: inputs.character,
      item: items,
      possessed: true
    });

    let maps = itemPossessions.map(async possession => {
      let criteria2 = {
        item: possession.item,
        character: clone.id,
        possessed: true,
        location: possession.location,
        HP: possession.HP,
        EP: possession.EP,
        XP: possession.XP,
        charges: possession.charges
      };

      await sails.models.septemitempossession.create(criteria2).fetch();
    });
    await Promise.all(maps);

    // All done.
    return { success: true };
  }
};
