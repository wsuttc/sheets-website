module.exports = {
  friendlyName: "septem/get/character",

  description: "Get a single character and subscribe to sockets.",

  inputs: {
    id: {
      type: "number",
      required: true,
      description: "ID of the character"
    },
    view: {
      type: "string",
      isIn: ["player", "operator", "gm"],
      defaultsTo: "player",
      description: "Determines what data will be available about the character."
    },
    password: {
      type: "string",
      defaultsTo: "",
      description: "The password for the view"
    }
  },

  exits: {},

  fn: async function(inputs) {
    // Check password
    if (inputs.view !== "player") {
      let password = await sails.models.septempasswords.findOne({
        name: inputs.view
      });
      if (!password || password.value !== inputs.password)
        return { error: "Wrong password" };
    }

    // Get the character
    let character = await sails.models.septemcharacters.findOne({
      id: inputs.id
    });
    // Error if no character found
    if (!character) return { error: "Character not found" };

    // Do not return inactive characters for non-gm views
    if (!character.active && inputs.view !== "gm")
      return {
        error:
          "That character is marked inactive; you cannot view inactive characters from non-GM panels."
      };

    // Subscribe to character socket
    if (this.req.isSocket) {
      sails.sockets.join(this.req, `septem-character-${inputs.id}`);
      sails.sockets.join(this.req, `septem-character-all`);
    }

    // Return stats for gms and PCs
    if (!character.isPrivate || inputs.view === "gm") {
      let stats = await sails.helpers.septem.stats(character.id);
      if (stats) {
        [
          "strength",
          "dexterity",
          "constitution",
          "intelligence",
          "wisdom",
          "charisma"
        ].forEach(key => {
          character[key] = {
            base: stats.baseStats[key],
            current: stats.currentStats[key],
            mod: stats.modifiers[key]
          };
        });
        character.armor = {
          base: stats.baseStats.armor,
          current: stats.currentStats.armor
        };
        character.movement = {
          base: stats.baseStats.movement,
          current: stats.currentStats.movement
        };
        character.maxHP = {
          base: stats.baseStats.maxHP,
          current: stats.currentStats.maxHP
        };
        character.maxEP = {
          base: stats.baseStats.maxEP,
          current: stats.currentStats.maxEP
        };
      }
      character.level = await sails.helpers.septem.level(character.XP);

      // Otherwise, do not return any stats
    } else {
      delete character.strength;
      delete character.dexterity;
      delete character.constitution;
      delete character.intelligence;
      delete character.wisdom;
      delete character.charisma;
      delete character.armor;
      delete character.movement;
      delete character.XP;
      delete character.SRD;

      delete character.gender;
      delete character.age;
      delete character.height;
      delete character.weight;
    }

    // Delete stuff that non-gms should not know
    if (inputs.view !== "gm") {
      // Non-GMs should not know if a player has inspiration or the gm notes
      delete character.hasInspiration;
      delete character.gmNotes;
    }

    // Delete stuff we do not want to return for the operator CP
    if (inputs.view === "operator") {
      // No earth stats for operator
      delete character.earthHP;
      delete character.earthEP;

      // Only percentiles for NPCs
      if (character.isPrivate) {
        character.matrixHPPercent = character.maxHP.current
          ? Math.ceil((character.matrixHP / character.maxHP.current) * 100)
          : 0;
        character.matrixEPPercent = character.maxEP.current
          ? Math.ceil((character.matrixEP / character.maxEP.current) * 100)
          : 0;
        delete character.matrixHP;
        delete character.matrixEP;
        delete character.maxHP;
        delete character.maxEP;
      }
    }

    // Delete stuff we will not return if this is a player view
    if (inputs.view === "player") {
      // Players should not know their matrix HP/EP; that must be communicated by the supercomputer operator.
      delete character.matrixHP;
      delete character.matrixEP;

      // Players should not know their SP; this is also communicated by the supercomputer operator.
      delete character.SP;
    }

    // Buffs are not visible nor reported on the supercomputer. Everyone else, including players, may view buffs.
    if (inputs.view !== "operator") {
      character.buffs = await sails.models.septemcharacterbuffs.find({
        character: inputs.id
      });
    }

    // We should not return other data such as items for NPCs on player panels
    if (!character.isPrivate || inputs.view !== "player") {
      // Get items the character currently possesses
      let itemsPossessed = await sails.models.septemitempossession.find({
        character: inputs.id
      });

      // Process buffs for each possession
      let maps = itemsPossessed.map(async itemPossessed => {
        // Get buffs for this possession
        itemPossessed.buffs = await sails.models.septemitembuffs.find({
          possession: itemPossessed.id
        });

        return itemPossessed;
      });
      itemsPossessed = await Promise.all(maps);

      // Get items
      character.items = await sails.models.septemitems.find();
      character.items = character.items.map(item => {
        item.possession = [];
        return item;
      });

      // Inject possessions into items
      itemsPossessed.forEach(possession => {
        let item = character.items.findIndex(
          item => item.id === possession.item
        );
        if (item > -1) character.items[item].possession.push(possession);
      });

      // Remove items with no possessions
      character.items = character.items.filter(
        item => item.possession.length > 0
      );
    }

    // Return the character
    return character;
  }
};
