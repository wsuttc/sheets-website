module.exports = {
  friendlyName: "septem/get/characters",

  description:
    "Return an overview of the available characters based on criteria (and subscribe to sockets).",

  inputs: {
    view: {
      type: "string",
      isIn: ["player", "operator", "gm"],
      defaultsTo: "player",
      description:
        "If player, will not return NPCs, Matrix Stats, SP, nor CP. If operator, will also return Matrix stats, SP, and CP (only for the protagonist team). If GM, will return everything."
    },
    password: {
      type: "string",
      defaultsTo: "",
      description: "The password for the view"
    }
  },

  exits: {},

  fn: async function(inputs) {
    // Check password
    if (inputs.view !== "player") {
      let password = await sails.models.septempasswords.findOne({
        name: inputs.view
      });
      if (!password || password.value !== inputs.password)
        return { error: "Wrong password" };
    }

    // Subscribe to websockets
    if (this.req.isSocket) {
      sails.sockets.join(this.req, `septem-characters`);
    }

    // Build search query; do not return inactive characters if the view is not gm
    let query = {};
    if (inputs.view !== "gm") query.active = true;

    // Get characters
    let characters = await sails.models.septemcharacters.find(query);

    // Return empty if no characters
    if (characters.length === 0) return [];

    characters = characters.map(async character => {
      // Delete stuff we will never use in overviews (like profile info / long text stuff)
      delete character.appearance;
      delete character.personality;
      delete character.biography;
      delete character.strengths;
      delete character.weaknesses;
      delete character.gmNotes;
      delete character.gender;
      delete character.age;
      delete character.height;
      delete character.weight;

      // Return stats for gms and PCs
      if (!character.isPrivate || inputs.view === "gm") {
        let stats = await sails.helpers.septem.stats(character.id);
        if (stats) {
          [
            "strength",
            "dexterity",
            "constitution",
            "intelligence",
            "wisdom",
            "charisma"
          ].forEach(key => {
            character[key] = {
              base: stats.baseStats[key],
              current: stats.currentStats[key],
              mod: stats.modifiers[key]
            };
          });
          character.armor = {
            base: stats.baseStats.armor,
            current: stats.currentStats.armor
          };
          character.movement = {
            base: stats.baseStats.movement,
            current: stats.currentStats.movement
          };
          character.maxHP = {
            base: stats.baseStats.maxHP,
            current: stats.currentStats.maxHP
          };
          character.maxEP = {
            base: stats.baseStats.maxEP,
            current: stats.currentStats.maxEP
          };
        }
        character.level = await sails.helpers.septem.level(character.XP);

        // Otherwise, do not return any stats
      } else {
        delete character.strength;
        delete character.dexterity;
        delete character.constitution;
        delete character.intelligence;
        delete character.wisdom;
        delete character.charisma;
        delete character.armor;
        delete character.movement;
        delete character.XP;
        delete character.SRD;
      }

      return character;
    });
    characters = await Promise.all(characters);

    // Delete stuff that non-gms should not know
    if (inputs.view !== "gm") {
      characters = characters.map(character => {
        // Only GMs should know who has inspiration
        delete character.hasInspiration;
        return character;
      });
    }

    // Delete stuff we do not want to return for the operator CP
    if (inputs.view === "operator") {
      characters = characters.map(character => {
        // No earth stats for operator
        delete character.earthHP;
        delete character.earthEP;

        // Only percentiles for NPCs
        if (character.isPrivate) {
          character.matrixHPPercent = character.maxHP.current
            ? Math.ceil((character.matrixHP / character.maxHP.current) * 100)
            : 0;
          character.matrixEPPercent = character.maxEP.current
            ? Math.ceil((character.matrixEP / character.maxEP.current) * 100)
            : 0;
          delete character.matrixHP;
          delete character.matrixEP;
          delete character.maxHP;
          delete character.maxEP;
        }

        return character;
      });
    }

    // Delete stuff we will not return if this is a player panel
    if (inputs.view === "player") {
      characters = characters.map(character => {
        // Players should not know their matrix HP/EP; that must be communicated by the supercomputer operator.
        delete character.matrixHP;
        delete character.matrixEP;

        // Players should not know their SP; this is also communicated by the supercomputer operator.
        delete character.SP;

        // Stuff to delete for NPCs on the player panel
        if (character.isPrivate) {
          // We also do not want them knowing ANY of their HP/EP, including Earth and maximum.
          delete character.earthHP;
          delete character.maxHP;
          delete character.earthEP;
          delete character.maxEP;
        }
        return character;
      });
    }

    return characters;
  }
};
