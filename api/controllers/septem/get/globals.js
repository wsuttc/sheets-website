const moment = require("moment");

module.exports = {
  friendlyName: "septem/get/globals",

  description: "Get global values / information for Septem.",

  inputs: {
    view: {
      type: "string",
      isIn: ["player", "operator", "gm", "scanner"],
      defaultsTo: "player",
      description:
        "Determines which sockets we subscribe to and thus what data is made available."
    },
    password: {
      type: "string",
      defaultsTo: "",
      description: "The password for the view"
    }
  },

  exits: {},

  fn: async function(inputs) {
    // Check password
    if (inputs.view !== "player") {
      let password = await sails.models.septempasswords.findOne({
        name: inputs.view
      });
      if (!password || password.value !== inputs.password)
        return { error: "Wrong password" };
    }

    // Get the globals
    let data = await sails.models.septemglobals.findOne({ id: 1 });
    if (!data) return {};

    // Subscribe to socket
    if (this.req.isSocket) {
      sails.sockets.join(this.req, `septem-globals`);
    }

    // Calculate inactive CCUs
    data.blueCCUs =
      data.totalCCUs -
      data.redCCUs -
      data.orangeCCUs -
      data.yellowCCUs -
      data.greenCCUs -
      data.indigoCCUs -
      data.violetCCUs -
      data.whiteCCUs;

    // Convert time to a string
    data.time = moment.unix(data.time).format("dddd, MMMM Do YYYY, h:mm:ss a");

    // Return everything for GMs
    if (inputs.view === "gm") return data;

    // Return only requiemSP and computaCP for operator
    if (inputs.view === "operator")
      return {
        time: data.time,
        requiemSP: data.requiemSP,
        computaCP: data.computaCP
      };

    // Return CCU data for scanner
    if (inputs.view === "scanner")
      return {
        redCCUs: data.redCCUs,
        orangeCCUs: data.orangeCCUs,
        yellowCCUs: data.yellowCCUs,
        greenCCUs: data.greenCCUs,
        blueCCUs: data.blueCCUs,
        indigoCCUs: data.indigoCCUs,
        violetCCUs: data.violetCCUs,
        whiteCCUs: data.whiteCCUs
      };

    // Only return campaign time for players
    if (inputs.view === "player")
      return {
        time: data.time
      };

    // If we somehow get here, return empty object.
    return {};
  }
};
