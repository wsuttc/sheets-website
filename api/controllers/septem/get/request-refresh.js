module.exports = {
  friendlyName: "septem/get/request-refresh",

  description: "Send web sockets to trigger data refresh.",

  inputs: {
    view: {
      type: "string",
      isIn: ["operator", "gm", "scanner"],
      required: true,
      description: "Which view are we executing from?"
    },
    password: {
      type: "string",
      defaultsTo: "",
      description: "The password for the view"
    }
  },

  fn: async function(inputs) {
    // Check password
    let password = await sails.models.septempasswords.findOne({
      name: inputs.view
    });
    if (!password || password.value !== inputs.password)
      return { error: "Wrong password" };

      // Trigger refreshes
      sails.sockets.broadcast(
        [`septem-character-all`, `septem-characters`],
        "septem-character-refresh",
        "all"
      );

      sails.sockets.broadcast(
        `septem-globals`,
        "septem-globals-refresh",
        "all"
      );

      return;
  }
};
