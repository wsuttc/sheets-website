module.exports = {
  friendlyName: "septem.Level",

  description: "Determine what level a character is on based on XP.",

  inputs: {
    XP: {
      type: "number",
      required: true,
      description: "Amount of XP to calculate a level."
    }
  },

  fn: async function(inputs) {
    return 1 + Math.floor(inputs.XP ** (1 / 1.275) / 100);
  }
};
