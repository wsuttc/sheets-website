module.exports = {
  friendlyName: "septem.statModifier",

  description:
    "Calculate the current stats of a character. Returns base stats, modified stats, and modifiers.",

  inputs: {
    id: {
      type: "number",
      required: true,
      description: "The character id to calculate"
    }
  },

  fn: async function(inputs) {
    // Get the character
    let character = await sails.models.septemcharacters.findOne({
      id: inputs.id
    });
    if (!character) return null;

    // Initialize stats
    let currentStats = {
      strength: character.strength,
      dexterity: character.dexterity,
      constitution: character.constitution,
      intelligence: character.intelligence,
      wisdom: character.wisdom,
      charisma: character.charisma,
      armor: character.armor,
      movement: character.movement,
      maxHP:
        character.maxHP !== null
          ? character.maxHP
          : 35 +
            (character.constitution / 2) *
              (await sails.helpers.septem.level(character.XP)),
      maxEP:
        character.maxEP !== null
          ? character.maxEP
          : (await sails.helpers.septem.level(character.XP)) * 2 + 23
    };
    let baseStats = _.clone(currentStats);

    // Factor in character buffs
    for (let key in currentStats) {
      let value = await sails.models.septemcharacterbuffs.sum(key, {
        character: inputs.id
      });
      if (value !== null) currentStats[key] += value;
    }

    // Factor in item modifiers
    let possessedItems = await sails.models.septemitempossession.find({
      character: inputs.id,
      possessed: true
    });
    if (possessedItems.length > 0) {
      let maps = possessedItems.map(async possessedItem => {
        // Initial item modifiers
        let item = await sails.models.septemitems.findOne({
          id: possessedItem.item
        });
        if (item) {
          for (let key in currentStats) {
            if (typeof item[key] !== "undefined" && item[key] !== null)
              currentStats[key] += item[key];
          }

          // Item buff modifiers
          let buffs = await sails.models.septemitembuffs.find({
            possession: possessedItem.id
          });
          if (buffs.length > 0) {
            buffs.forEach(buff => {
              for (let key in currentStats) {
                // Do not apply buff modifiers if original item does not modify from the start
                if (item[key] !== null && buff[key])
                  currentStats[key] += buff[key];
              }
            });
          }
        }
      });
      await Promise.all(maps);
    }

    // Calculate modifiers
    let modifiers = {
      strength: Math.floor((currentStats.strength - 10) / 2),
      dexterity: Math.floor((currentStats.dexterity - 10) / 2),
      constitution: Math.floor((currentStats.constitution - 10) / 2),
      intelligence: Math.floor((currentStats.intelligence - 10) / 2),
      wisdom: Math.floor((currentStats.wisdom - 10) / 2),
      charisma: Math.floor((currentStats.charisma - 10) / 2)
    };

    return { baseStats, currentStats, modifiers };
  }
};
