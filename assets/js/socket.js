// Initialize websocket connection to the Sails server so we can get notified when things change on the website

// Allow both polling and websocket transports (tries websocket, falls back to polling)
io.sails.transports = ["polling", "websocket"];

// Always try to reconnect when disconnected
io.sails.reconnection = true;

// NOTE: You should call io.sails.connect() in the respective page script
// Example: var socket = io.sails.connect();