// This file is used for managing characters on overview panels

// Manager for all characters. Use this.
class SeptemOverviewCharacters {
  /**
   * Construct the class
   *
   * @param {string} players DOM query string of the container where player characters should go
   * @param {string} NPCs DOM query string of the container where NPCs should go
   * @param {sails.io} socket sails.io socket to make requests to the API
   */
  constructor(players, NPCs, socket) {
    // Store the DOM strings
    this.dom = {
      players: players,
      NPCs: NPCs
    };

    // Construct a map for our characters
    this.characters = new Map();

    this.socket = socket;
  }

  /**
   * Process data from septem/get/characters
   *
   * @param {Array} data Array of characters
   */
  process(data) {
    // Remember which characters currently exist so we can remove old ones.
    let existing = [];

    // Process existing characters
    if (data.length > 0) {
      data.forEach(character => {
        existing.push(character.id);

        // new character? Create a card for it
        if (!this.characters.has(character.id)) {
          console.log(`New character: ${character.id}`);
          this.add(character);
          return;
        }

        // Character private status changed? Remove and re-create the character.
        let prevCharacter = this.characters.get(character.id);
        if (
          prevCharacter &&
          prevCharacter.data.isPrivate !== character.isPrivate
        ) {
          console.log(`Character private status changed: ${character.id}`);
          this.remove(character.id);
          this.add(character);
          return;
        }

        // Beyond this point, update character with new data
        if (prevCharacter) {
          prevCharacter.update(character);
        }
      });
    }

    // Remove characters that no longer exist
    let memoryCharacters = Array.from(this.characters.keys());
    console.dir(memoryCharacters);
    console.dir(existing);
    if (memoryCharacters.length > 0) {
      memoryCharacters.forEach(char => {
        if (existing.indexOf(char) === -1) {
          console.log(`Did not find ${char} anymore.`);
          this.remove(char);
        }
      });
    }
  }

  /**
   * Create a new character
   *
   * @param {object} character Character data
   */
  add(character) {
    this.characters.set(
      character.id,
      new SeptemOverviewCharacter(
        character.isPrivate ? this.dom.NPCs : this.dom.players,
        character,
        socket
      )
    );
  }

  /**
   * Remove a character.
   *
   * @param {number} id ID of the character to remove
   */
  remove(id) {
    if (this.characters.has(id)) {
      console.log(`Removing character ${id}`);
      let character = this.characters.get(id);
      character.remove(); // Remove the card
      this.characters.delete(id);
    }
  }

  /**
   * Get a character.
   *
   * @param {number} id ID of the character to fetch
   */
  get(id) {
    return this.characters.get(id);
  }
}

// Manager for a single character. Do NOT use this directly; use SeptemOverviewCharacters instead.
class SeptemOverviewCharacter {
  /**
   * Construct the class
   *
   * @param {string} dom The DOM query string of the container this character should go.
   * @param {object} data Initial data for the character
   * @param {sails.io} socket sails.io socket to make requests to the API
   */
  constructor(dom, data, socket) {
    this.dom = dom;
    this.data = data; // Used for storing a current record of the character data.
    this.socket = socket;

    // Make the card
    this.makeCard(this.dom);

    // On next frame, update card with data
    window.requestAnimationFrame(() => {
      this.update(this.data, false);
    });
  }

  /**
   * Construct the character card.
   *
   * @param {string} dom The DOM to append the card in.
   */
  makeCard(dom) {
    $(dom).append(`<div
        class="col-xxl-3 col-lg-4 col-sm-6 col-12"
        id="septem-character-${this.data.id}"
      >
        <div
          class="position-relative card card-secondary card-outline"
          id="septem-character-${this.data.id}-card"
        >
          <div
            class="ribbon-wrapper ribbon-lg d-none"
            id="septem-character-${this.data.id}-level"
            style="pointer-events: none;"
          >
            <div class="ribbon bg-success septem-edit" data-key="XP" style="pointer-events: auto;">
              LEVEL ???
            </div>
          </div>
          <div
            class="ribbon-wrapper ribbon-xl d-none" 
            id="septem-character-${this.data.id}-tag"
            style="pointer-events: none;"
          >
            <div class="ribbon bg-warning septem-edit" data-key="tag" style="pointer-events: auto;">
              MINI BOSS
            </div>
          </div>
          <div
          class="ribbon-wrapper d-none" 
          id="septem-character-${this.data.id}-hasinspiration"
          title="This character has secret inspiration if the ribbon is lime green and reads *INS* with asterisks"
          style="pointer-events: none;"
        >
          <div class="ribbon bg-lime septem-edit" data-key="hasInspiration" style="pointer-events: auto;">
            *INS*
          </div>
        </div>
          <div class="card-body box-profile">
            <div class="text-center septem-edit" id="septem-character-${
              this.data.id
            }-image" title="Character ID: ${this.data.id}" data-key="image">
              <div
                class="profile-user-img img-fluid img-circle bg-secondary"
                style="font-size: 48px;"
              >
              ${this.data.id}
              </div>
            </div>

            <h3
              class="profile-username text-center septem-edit"
              id="septem-character-${this.data.id}-name"
              data-key="name"
            >
              (Name Redacted)
            </h3>

            <p
              class="text-muted text-center septem-edit"
              id="septem-character-${this.data.id}-class"
              data-key="class"
            >
              (Class Redacted)
            </p>

            <div class="row border">
              <div class="col-sm-6 col-md-4 col-xxl-2">
                <div class="description-block">
                  <h5
                    class="description-header septem-edit"
                    id="septem-character-${this.data.id}-strength"
                    data-key="strength"
                  >
                    ???
                  </h5>
                  <span class="description-text">STR</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-6 col-md-4 col-xxl-2">
                <div class="description-block">
                  <h5
                    class="description-header septem-edit"
                    id="septem-character-${this.data.id}-dexterity"
                    data-key="dexterity"
                  >
                    ???
                  </h5>
                  <span class="description-text">DEX</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-6 col-md-4 col-xxl-2">
                <div class="description-block">
                  <h5
                    class="description-header septem-edit"
                    id="septem-character-${this.data.id}-constitution"
                    data-key="constitution"
                  >
                    ???
                  </h5>
                  <span class="description-text">CON</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-6 col-md-4 col-xxl-2">
                <div class="description-block">
                  <h5
                    class="description-header septem-edit"
                    id="septem-character-${this.data.id}-intelligence"
                    data-key="intelligence"
                  >
                    ???
                  </h5>
                  <span class="description-text">INT</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-6 col-md-4 col-xxl-2">
                <div class="description-block">
                  <h5
                    class="description-header septem-edit"
                    id="septem-character-${this.data.id}-wisdom"
                    data-key="wisdom"
                  >
                    ???
                  </h5>
                  <span class="description-text">WIS</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
              <div class="col-sm-6 col-md-4 col-xxl-2">
                <div class="description-block">
                  <h5
                    class="description-header septem-edit"
                    id="septem-character-${this.data.id}-charisma"
                    data-key="charisma"
                  >
                    ???
                  </h5>
                  <span class="description-text">CHA</span>
                </div>
                <!-- /.description-block -->
              </div>
              <div class="col-sm-6 col-md-4 col-xxl-2">
                <div class="description-block">
                  <h5
                    class="description-header septem-edit"
                    id="septem-character-${this.data.id}-armor"
                    data-key="armor"
                  >
                    ???
                  </h5>
                  <span class="description-text">ARMOR</span>
                </div>
                <!-- /.description-block -->
              </div>
              <div class="col-sm-6 col-md-4 col-xxl-2">
                <div class="description-block">
                  <h5
                    class="description-header septem-edit"
                    id="septem-character-${this.data.id}-movement"
                    data-key="movement"
                  >
                    ???
                  </h5>
                  <span class="description-text">MOVE</span>
                </div>
                <!-- /.description-block -->
              </div>
              <div class="col-sm-6 col-md-4 col-xxl-2">
                <div class="description-block">
                  <h5
                    class="description-header septem-edit"
                    id="septem-character-${this.data.id}-SRD"
                    data-key="SRD"
                  >
                    $?.??
                  </h5>
                  <span class="description-text">SRD</span>
                </div>
                <!-- /.description-block -->
              </div>
              <!-- /.col -->
            </div>

            <hr />

            <div class="row">
              <div class="col-6">
                <h5>Earth</h5>
                <div class="progress septem-edit" data-key="earthHP">
                  <div
                    class="progress-bar bg-danger"
                    role="progressbar"
                    style="width: 0%"
                    id="septem-character-${this.data.id}-earthHP-progress"
                  ></div>
                </div>
                <small class="septem-edit" data-key="maxHP" id="septem-character-${
                  this.data.id
                }-earthHP"
                  >??? / ??? HP</small
                >
                <div class="progress septem-edit" data-key="earthEP">
                  <div
                    class="progress-bar bg-success"
                    role="progressbar"
                    style="width: 0%"
                    id="septem-character-${this.data.id}-earthEP-progress"
                  ></div>
                </div>
                <small class="septem-edit" data-key="maxEP" id="septem-character-${
                  this.data.id
                }-earthEP"
                  >??? / ??? EP</small
                >
              </div>
              <div class="col-6">
                <h5>Matrix</h5>
                <div class="progress septem-edit" data-key="matrixHP">
                  <div
                    class="progress-bar bg-danger"
                    role="progressbar"
                    style="width: 0%"
                    id="septem-character-${this.data.id}-matrixHP-progress"
                  ></div>
                </div>
                <small class="septem-edit" data-key="maxHP" id="septem-character-${
                  this.data.id
                }-matrixHP"
                  >??? / ??? HP</small
                >
                <div class="progress septem-edit" data-key="matrixEP">
                  <div
                    class="progress-bar bg-success"
                    role="progressbar"
                    style="width: 0%"
                    id="septem-character-${this.data.id}-matrixEP-progress"
                  ></div>
                </div>
                <small class="septem-edit" data-key="maxEP" id="septem-character-${
                  this.data.id
                }-matrixEP"
                  >??? / ??? EP</small
                >
              </div>
            </div>

            <hr />

            <a type="button" class="btn btn-primary btn-block" id="septem-character-${
              this.id
            }-sheet" href="/septem/${window.pageView}/${
      window.pagePassword ? `${window.pagePassword}/` : ``
    }character/${this.data.id}" target="_blank">View Full Sheet</a>
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
        
      </div>`);
  }

  /**
   * Call this before removing the character from the manager so that the card is removed.
   */
  remove() {
    $(`#septem-character-${this.data.id}`).remove();
  }

  /**
   * Update character data
   *
   * @param {object} data New character data
   * @param {?boolean} showToasts Show toasts for changes in stats? (Default = true)
   */
  update(data, showToasts = true) {
    let changes = []; // Keep track of everything that changed

    // First, update what we do know
    for (let key in data) {
      // Calculate changes
      if (
        [
          "strength",
          "dexterity",
          "constitution",
          "intelligence",
          "wisdom",
          "charisma",
          "armor",
          "movement",
          "maxHP",
          "maxEP"
        ].indexOf(key) !== -1
      ) {
        if (
          typeof this.data[key] === "undefined" ||
          this.data[key] === null ||
          (typeof data[key] !== "undefined" &&
            data[key] !== null &&
            !shallowEqual(data[key], this.data[key]))
        )
          changes.push(
            `<strong>${key}</strong> changed to ${data[key].current}${
              typeof data[key].mod !== "undefined" ? ` (${data[key].mod})` : ``
            }`
          );
      } else {
        if (["buffs", "createdAt", "updatedAt", "items"].indexOf(key) === -1) {
          if (data[key] !== this.data[key])
            changes.push(`<strong>${key}</strong> changed to ${data[key]}.`);
        }
      }

      // Active status
      if (key === "active") {
        $(`#septem-character-${this.data.id}-card`).removeClass(
          "card-secondary"
        );
        $(`#septem-character-${this.data.id}-card`).removeClass("card-success");
        $(`#septem-character-${this.data.id}-card`).addClass(
          data[key] ? "card-success" : "card-secondary"
        );
        continue;
      }

      // Images
      if (key === "image") {
        if (data[key] !== null) {
          $(`#septem-character-${this.data.id}-${key}`).html(`<img
          class="profile-user-img img-fluid img-circle bg-secondary"
          src="${data[key]}"
        >`);
        } else {
          $(`#septem-character-${this.data.id}-${key}`).html(`<div
          class="profile-user-img img-fluid img-circle bg-secondary"
          style="font-size: 48px;"
        >
        ${this.data.id}
        </div>`);
        }
        continue;
      }

      // Ribbons / tags
      if (key === "tag") {
        switch (data[key]) {
          case "Mini Boss":
            $(`#septem-character-${this.data.id}-tag`).removeClass("d-none");
            $(`#septem-character-${this.data.id}-tag`)
              .html(`<div class="ribbon bg-warning septem-edit" data-key="tag" style="pointer-events: auto;">
            MINI BOSS
          </div>`);
            break;
          case "Final Boss":
            $(`#septem-character-${this.data.id}-tag`).removeClass("d-none");
            $(`#septem-character-${this.data.id}-tag`)
              .html(`<div class="ribbon bg-danger septem-edit" data-key="tag" style="pointer-events: auto;">
            FINAL BOSS
          </div>`);
            break;
          case "Oversized Boss":
            $(`#septem-character-${this.data.id}-tag`).removeClass("d-none");
            $(`#septem-character-${this.data.id}-tag`)
              .html(`<div class="ribbon bg-orange septem-edit" data-key="tag" style="pointer-events: auto;">
            OVERSIZED BOSS
          </div>`);
            break;
          case "NPC":
            $(`#septem-character-${this.data.id}-tag`).removeClass("d-none");
            $(`#septem-character-${this.data.id}-tag`)
              .html(`<div class="ribbon bg-indigo septem-edit" data-key="tag" style="pointer-events: auto;">
            NPC
          </div>`);
            break;
          case "Object":
            $(`#septem-character-${this.data.id}-tag`).removeClass("d-none");
            $(`#septem-character-${this.data.id}-tag`)
              .html(`<div class="ribbon bg-secondary septem-edit" data-key="tag" style="pointer-events: auto;">
            OBJECT
          </div>`);
            break;
          case "Unknown Motive":
            $(`#septem-character-${this.data.id}-tag`).removeClass("d-none");
            $(`#septem-character-${this.data.id}-tag`)
              .html(`<div class="ribbon bg-info septem-edit" data-key="tag" style="pointer-events: auto;">
            UNKNOWN MOTIVE
          </div>`);
            break;
          case "Clone":
            $(`#septem-character-${this.data.id}-tag`).removeClass("d-none");
            $(`#septem-character-${this.data.id}-tag`)
              .html(`<div class="ribbon bg-teal septem-edit" data-key="tag" style="pointer-events: auto;">
              CLONE
            </div>`);
            break;
          default:
            $(`#septem-character-${this.data.id}-tag`).addClass("d-none");
        }
        continue;
      }

      // Level
      if (key === "level") {
        $(`#septem-character-${this.data.id}-level`).removeClass("d-none");
        $(`#septem-character-${this.data.id}-level`)
          .html(`<div class="ribbon bg-success septem-edit" data-key="XP" style="pointer-events: auto;">
        LEVEL ${data[key]}
        </div>`);
        continue;
      }

      // hasInspiration
      if (key === "hasInspiration") {
        $(`#septem-character-${this.data.id}-hasinspiration`).removeClass(
          "d-none"
        );
        if (data[key]) {
          $(`#septem-character-${this.data.id}-hasinspiration`)
            .html(`<div class="ribbon bg-lime septem-edit" data-key="hasInspiration" style="pointer-events: auto;">
          *INS*
        </div>`);
        } else {
          $(`#septem-character-${this.data.id}-hasinspiration`)
            .html(`<div class="ribbon bg-black septem-edit" data-key="hasInspiration" style="pointer-events: auto;">
          INS
        </div>`);
        }
        continue;
      }

      // Stats
      if (
        [
          "strength",
          "dexterity",
          "constitution",
          "intelligence",
          "wisdom",
          "charisma",
          "armor",
          "movement"
        ].indexOf(key) !== -1
      ) {
        $(`#septem-character-${this.data.id}-${key}`).html(
          `${data[key].current}${
            typeof data[key].mod !== "undefined" ? ` (${data[key].mod})` : ``
          }${key === "movement" ? ` m` : ``}`
        );
        continue;
      }

      // HP
      if (["earthHP", "matrixHP"].indexOf(key) !== -1) {
        $(`#septem-character-${this.data.id}-${key}`).html(
          `${data[key] || `???`} / ${
            typeof data.maxHP !== "undefined" ? data.maxHP.current : `???`
          } HP`
        );
        $(`#septem-character-${this.data.id}-${key}-progress`).css(
          "width",
          `${
            data.maxHP && data.maxHP.current
              ? (data[key] / data.maxHP.current) * 100
              : 0
          }%`
        );
        continue;
      }

      // EP
      if (["earthEP", "matrixEP"].indexOf(key) !== -1) {
        $(`#septem-character-${this.data.id}-${key}`).html(
          `${data[key] || `???`} / ${
            typeof data.maxEP !== "undefined" ? data.maxEP.current : `???`
          } EP`
        );
        $(`#septem-character-${this.data.id}-${key}-progress`).css(
          "width",
          `${
            data.maxEP && data.maxEP.current
              ? (data[key] / data.maxEP.current) * 100
              : 0
          }%`
        );
        continue;
      }

      // Max HP
      if (key === "maxHP") {
        ["earthHP", "matrixHP"].forEach(_key => {
          $(`#septem-character-${this.data.id}-${_key}`).html(
            `${data[_key] || `???`} / ${
              typeof data.maxHP !== "undefined" ? data.maxHP.current : `???`
            } HP`
          );
          $(`#septem-character-${this.data.id}-${_key}-progress`).css(
            "width",
            `${
              data.maxHP && data.maxHP.current
                ? (data[_key] / data.maxHP.current) * 100
                : 0
            }%`
          );
        });
        continue;
      }

      // Max EP
      if (key === "maxEP") {
        ["earthEP", "matrixEP"].forEach(_key => {
          $(`#septem-character-${this.data.id}-${_key}`).html(
            `${data[_key] || `???`} / ${
              typeof data.maxEP !== "undefined" ? data.maxEP.current : `???`
            } EP`
          );
          $(`#septem-character-${this.data.id}-${_key}-progress`).css(
            "width",
            `${
              data.maxEP && data.maxEP.current
                ? (data[_key] / data.maxEP.current) * 100
                : 0
            }%`
          );
        });
        continue;
      }

      // Percents
      if (key === "matrixHPPercent") {
        $(`#septem-character-${this.data.id}-matrixHP`).html(`HP %`);
        $(`#septem-character-${this.data.id}-matrixHP-progress`).css(
          "width",
          `${data[key]}%`
        );
        continue;
      }
      if (key === "matrixEPPercent") {
        $(`#septem-character-${this.data.id}-matrixEP`).html(`EP %`);
        $(`#septem-character-${this.data.id}-matrixEP-progress`).css(
          "width",
          `${data[key]}%`
        );
        continue;
      }

      // SRD
      if (key === "SRD") {
        $(`#septem-character-${this.data.id}-${key}`).html(
          `$${data[key].toFixed(2)}`
        );
        continue;
      }

      // Standard for everything else
      $(`#septem-character-${this.data.id}-${key}`).html(
        !data[key] || data[key] === "" ? `(Not Set)` : data[key]
      );
    }

    // Set as new data
    this.data = data;

    // Update septem-edit click handlers on the next frame
    window.requestAnimationFrame(() => {
      // Editing inline
      if (window.pageView === "gm") {
        $(`#septem-character-${this.data.id} .septem-edit`).unbind("click");
        $(`#septem-character-${this.data.id} .septem-edit`).click(e => {
          let keyClicked = $(e.currentTarget).data("key");
          let defaultValue = this.data[keyClicked];

          this.editValue(keyClicked, defaultValue);
        });
      }
    });

    // Show toasts if applicable
    if (showToasts && changes.length > 0) {
      $(document).Toasts("create", {
        class: "bg-warning",
        title: `${this.data.name || `(Redacted Name)`} (${
          this.data.id
        }) Changed`,
        body: `<ul>${changes
          .map(change => `<li>${change}</li>`)
          .join("")}</ul>`,
        autohide: true,
        delay: 20000
      });
    }
  }

  /**
   * Open a modal with an Alpaca form to edit a value with the character.
   *
   * @param {string} key Name of key to edit
   * @param {string} placeholder Default value
   */
  editValue(key, placeholder) {
    // Set null placeholders to empty strings; they work better with Alpaca
    if (placeholder === null) placeholder = "";

    // If the placeholder is an object, it came from a stat; use its base property value as the placeholder
    if (typeof placeholder === "object") {
      if (typeof placeholder.base !== "undefined")
        placeholder = placeholder.base;
    }

    // Map keys to field types
    let typeMap = {
      active: "boolean",
      isPrivate: "boolean",
      name: "string",
      image: "string",
      hasinspiration: "boolean",
      class: "string",
      age: "string",
      gender: "string",
      height: "number",
      weight: "number",
      strength: "number",
      dexterity: "number",
      constitution: "number",
      intelligence: "number",
      wisdom: "number",
      charisma: "number",
      armor: "number",
      movement: "number",
      earthHP: "number",
      earthEP: "number",
      matrixHP: "number",
      matrixEP: "number",
      maxHP: "number",
      maxEP: "number",
      XP: "number",
      SRD: "number",
      SP: "number",
      personality: "tinymce",
      appearance: "tinymce",
      biography: "tinymce",
      strengths: "tinymce",
      weaknesses: "tinymce",
      gmNotes: "tinymce"
    };

    // Determine the field type for this key
    let theType = typeMap[key];

    // Pre-determine our field
    let properties = {
      value: {
        title: `${key} for ${this.data.name} (${this.data.id})`,
        type: theType,
        required: theType === "number"
      }
    };

    // Pre-determine our options
    let options = {
      fields: {},
      form: {
        buttons: {
          submit: {
            title: `Edit Value`,
            click: (form, e) => {
              form.refreshValidationState(true);
              if (!form.isValid(true)) {
                form.focus();
                return;
              }
              let value = form.getValue();

              let update = {};
              update[key] = value.value;
              update.token = window.pagePassword;

              // If adjust was set, adjust the new value by the value of adjust
              if (typeof value.adjust !== "undefined")
                update[key] += value.adjust;

              this.socket.patch(
                `/septemcharacters/${this.data.id}`,
                update,
                resData => {
                  $("#modal-edit-value").modal("hide");
                }
              );
            }
          }
        }
      }
    };

    // If field type is tinymce, set tinymce options
    if (theType === "tinymce") {
      theType = "string";
      options.fields.value = {
        type: "tinymce",
        options: {
          toolbar:
            "undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | fullscreen preview | image link | ltr rtl",
          plugins:
            "autoresize preview paste importcss searchreplace autolink save directionality visualblocks visualchars fullscreen image link table hr pagebreak nonbreaking toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help quickbars",
          menubar: "file edit view insert format tools table help"
        }
      };
    }

    // If field type is a number, add adjust field
    if (theType === "number") {
      properties.adjust = {
        title: `-OR- Adjust ${key} by`,
        type: "number",
        default: 0
      };
    }

    // Create form
    $(`#modal-edit-value .modal-body`).html("");
    $(`#modal-edit-value .modal-body`).alpaca({
      schema: {
        type: "object",
        properties: properties
      },
      options: options,
      data: { value: placeholder }
    });

    $(`#modal-edit-value .modal-footer`).html("");

    $("#modal-edit-value").modal("show");
  }
}
// Helper
function shallowEqual(object1, object2) {
  const keys1 = Object.keys(object1);
  const keys2 = Object.keys(object2);

  if (keys1.length !== keys2.length) {
    return false;
  }

  for (let key of keys1) {
    if (object1[key] !== object2[key]) {
      return false;
    }
  }

  return true;
}
