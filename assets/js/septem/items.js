// This manages character items on the character sheet

class SeptemCharacterItems {
  /**
   * Construct the class
   *
   * @param {string} items DOM query string of the container where items should be listed
   * @param {string} modal DOM query string of the modal to use when clicking a item for more information
   * @param {sails.io} socket sails.io socket to make requests to the API
   */
  constructor(items, modal, socket) {
    // Store the DOM strings
    this.dom = {
      items: items,
      modal: modal
    };

    // Construct a map for our items
    this.items = new Map();

    this.socket = socket;
  }

  /**
   * Process data from septem/get/character
   *
   * @param {Array} data Result of character.items
   */
  process(data) {
    // Remember which items currently exist so we can remove old ones.
    let existing = [];

    // Process existing items
    if (data.length > 0) {
      data.forEach(item => {
        existing.push(item.id);

        // new item? Create a record for it
        if (!this.items.has(item.id)) {
          console.log(`New item: ${item.id}`);
          this.add(item);
          return;
        }

        let previtem = this.items.get(item.id);

        // Beyond this point, update item with new data
        if (previtem) {
          previtem.update(item);
        }
      });
    }

    // Remove items that no longer exist
    let memoryitems = Array.from(this.items.keys());
    console.dir(memoryitems);
    console.dir(existing);
    if (memoryitems.length > 0) {
      memoryitems.forEach(itm => {
        if (existing.indexOf(itm) === -1) {
          console.log(`Did not find item ${itm} anymore.`);
          this.remove(itm);
        }
      });
    }
  }

  /**
   * Create a new item
   *
   * @param {object} item item data
   */
  add(item) {
    this.items.set(
      item.id,
      new SeptemCharacterItem(this.dom.items, item, socket)
    );
  }

  /**
   * Remove a item.
   *
   * @param {number} id ID of the item to remove
   */
  remove(id) {
    if (this.items.has(id)) {
      console.log(`Removing item ${id}`);
      let item = this.items.get(id);
      item.remove(); // Remove the card
      this.items.delete(id);
    }
  }

  /**
   * Get a item.
   *
   * @param {number} id ID of the item to fetch
   */
  get(id) {
    return this.items.get(id);
  }
}

// Manager for a single item. Do NOT use this directly; use SeptemCharacteritems instead.
class SeptemCharacterItem {
  /**
   * Construct the class
   *
   * @param {string} dom The DOM query string of the container this item should go.
   * @param {object} data Initial data for the item
   * @param {sails.io} socket sails.io socket to make requests to the API
   */
  constructor(dom, data, socket) {
    this.dom = dom;
    this.data = data; // Used for storing a current record of the item data.
    this.socket = socket;

    // Make the record
    this.makeRecord(this.dom);

    // On next frame, update record with data
    window.requestAnimationFrame(() => {
      this.update(this.data, false);
    });
  }

  /**
   * Construct the item record.
   *
   * @param {string} dom The DOM to append the record in.
   */
  makeRecord(dom) {
    $(dom).append(
      `<div class="p-3" id="septem-characteritem-${this.data.id}">
      <h4><span class="p-1 badge" id="septem-characteritem-${this.data.id}-type"></span><strong id="septem-characteritem-${this.data.id}-name"></strong></h4>
      <div class="container-fluid" id="septem-characteritem-${this.data.id}-possession"></div>
      </div>`
    );

    // Click event
    $(`#septem-characteritem-${this.data.id}`).unbind("click");
    $(`#septem-characteritem-${this.data.id}`).click(() => {
      this.showMoreInfo();
    });
  }

  /**
   * Call this before removing the character from the manager so that the card is removed.
   */
  remove() {
    $(`#septem-characteritem-${this.data.id}`).remove();
  }

  /**
   * Update character data
   *
   * @param {object} data New character data
   * @param {?boolean} showToasts Show toasts for changes in stats? (Default = true)
   */
  update(data, showToasts = true) {
    let changes = []; // Keep track of everything that changed

    // First, update what we do know
    for (let key in data) {
      // Editing inline
      if (window.pageView === "gm") {
        $(`#septem-characteritem-${this.data.id} .septem-edit`).unbind("click");
        $(`#septem-characteritem-${this.data.id} .septem-edit`).click(e => {
          let keyClicked = $(e.currentTarget).data("key");
          let defaultValue = this.data[keyClicked];

          this.editValue(keyClicked, defaultValue);
        });
      }

      // Type
      if (key === "type") {
        let badgeMap = {
          Weapon: "badge-danger",
          Ability: "badge-warning",
          Spell: "bg-lime",
          Feature: "badge-info",
          Artifact: "bg-indigo",
          Armor: "badge-secondary",
          Food: "badge-success",
          Drink: "badge-success",
          Vehicle: "bg-purple",
          Equipment: "bg-purple",
          Medicine: "badge-success",
          Software: "bg-teal",
          Hardware: "bg-teal"
        };
        $(`#septem-characteritem-${this.data.id}-${key}`).attr(
          "class",
          `p-1 m-1 badge ${badgeMap[data[key]]}`
        );
        $(`#septem-characteritem-${this.data.id}-${key}`).html(data[key]);
        continue;
      }

      // Possession
      if (key === "possession") {
        $(`#septem-characteritem-${this.data.id}-${key}`).html(``);
        if (data[key].length > 0) {
          data[key].forEach(possession => {
            $(`#septem-characteritem-${this.data.id}-${key}`).append(
              `<div class="row border-top border-left border-right">
                <div class="col-auto">
                <div class="description-block">
                <h5 class="description-header">${possession.id}</h5>
                <span class="description-text">Possession ID</span>
                </div>
                </div>
                <div class="col-auto"><div class="description-block">
                <h5 class="description-header septem-itempossession-edit" data-possession="${possession.id}" data-key="possessed">${possession.possessed ? `YES` : `NO`}</h5>
                <span class="description-text">Equipped?</span>
                </div>
                </div>
                <div class="col-auto">
                ${
                  possession.HP !== null
                    ? `<div class="description-block">
                <h5 class="description-header septem-itempossession-edit" data-possession="${possession.id}" data-key="HP">${possession.HP} / ${this.data.HP}</h5>
                <span class="description-text">HP</span>
              </div>`
                    : ``
                }
                </div>
                <div class="col-auto">
                ${
                  possession.EP !== null
                    ? `<div class="description-block">
                <h5 class="description-header septem-itempossession-edit" data-possession="${possession.id}" data-key="EP">${possession.EP} / ${this.data.EP}</h5>
                <span class="description-text">EP</span>
              </div>`
                    : ``
                }
                </div>
                <div class="col-auto">
                ${
                  possession.charges !== null
                    ? `<div class="description-block">
                <h5 class="description-header septem-itempossession-edit" data-possession="${possession.id}" data-key="charges">${possession.charges} / ${this.data.charges}</h5>
                <span class="description-text">Charges</span>
              </div>`
                    : ``
                }
                </div>
                <div class="col-auto">
                ${
                  possession.XP
                    ? `<div class="description-block">
                <h5 class="description-header septem-itempossession-edit" data-possession="${possession.id}" data-key="XP">${possession.XP}</h5>
                <span class="description-text">XP</span>
              </div>`
                    : ``
                }
                </div>
              </div>
              ${
                possession.buffs && possession.buffs.length > 0
                  ? `<div class="row border-left border-right"><strong>BUFFS:</strong><ul>
              ${possession.buffs
                .map(
                  buff =>
                    `<li><strong>${buff.name}</strong> (${
                      buff.duration !== null
                        ? `${buff.duration} minutes remaining`
                        : `Untimed`
                    })</li>`
                )
                .join("")}</ul></div>`
                  : ``
              }
              <div class="row border-left border-right border-bottom">
              ${
                possession.location
                  ? `<strong>LOCATION:</strong> ${possession.location}`
                  : `<strong>LOCATION:</strong> Being carried or N/A`
              }
              </div>
              `
            );
          });
        }
        continue;
      }

      // Standard for everything else
      $(`#septem-characteritem-${this.data.id}-${key}`).html(
        !data[key] || data[key] === "" ? `(Not Set)` : data[key]
      );
    }

    // Set as new data
    this.data = data;

    // Show toasts if applicable
    if (showToasts && changes.length > 0) {
      $(document).Toasts("create", {
        class: "bg-warning",
        title: `${this.data.name || `(Redacted Name)`} (${
          this.data.id
        }) Changed`,
        body: `<ul>${changes
          .map(change => `<li>${change}</li>`)
          .join("")}</ul>`,
        autohide: true,
        delay: 20000
      });
    }
  }

  /**
   * Open a modal with an Alpaca form to edit a value with the character.
   *
   * @param {string} key Name of key to edit
   * @param {string} placeholder Default value
   */
  editValue(key, placeholder) {
    // TODO
    /*
      if (placeholder === null) placeholder = "";
      if (typeof placeholder === "object") {
        if (typeof placeholder.base !== "undefined")
          placeholder = placeholder.base;
      }
  
      let typeMap = {
        active: "boolean",
        isPrivate: "boolean",
        name: "string",
        image: "string",
        hasinspiration: "boolean",
        class: "string",
        height: "number",
        weight: "number",
        strength: "number",
        dexterity: "number",
        constitution: "number",
        intelligence: "number",
        wisdom: "number",
        charisma: "number",
        armor: "number",
        movement: "number",
        earthHP: "number",
        earthEP: "number",
        matrixHP: "number",
        matrixEP: "number",
        maxHP: "number",
        maxEP: "number",
        HP: "number",
        EP: "number",
        XP: "number",
        SRD: "number",
        SP: "number"
      };
  
      // Create form
      $(`#modal-edit-value .modal-body`).html("");
      $(`#modal-edit-value .modal-body`).alpaca({
        schema: {
          type: "object",
          properties: {
            value: {
              title: `${key} for ${this.data.name} (${this.data.id})`,
              type: typeMap[key],
              required: typeMap[key] === "number"
            }
          }
        },
        options: {
          form: {
            buttons: {
              submit: {
                title: `Edit Value`,
                click: (form, e) => {
                  form.refreshValidationState(true);
                  if (!form.isValid(true)) {
                    form.focus();
                    return;
                  }
                  let value = form.getValue();
  
                  let update = {};
                  update[key] = value.value;
                  update.token = window.pagePassword;
  
                  this.socket.patch(
                    `/septemcharacters/${this.data.id}`,
                    update,
                    resData => {
                      $("#modal-edit-value").modal("hide");
                    }
                  );
                }
              }
            }
          }
        },
        data: { value: placeholder }
      });
  
      $("#modal-edit-value").modal("show");
      */
  }

  showMoreInfo() {
    $(`#modal-edit-value .modal-body`).html(
      `<h1>${this.data.name} (${this.data.id})</h1>${this.data.info}`
    );
    if (this.data.actionLink) {
      $(`#modal-edit-value .modal-footer`).html(
        `<button type="button" class="btn btn-block btn-primary modal-button-action">Action</button>`
      );
      window.requestAnimationFrame(() => {
        $(`#modal-edit-value .modal-footer .modal-button-action`).unbind(
          "click"
        );
        $(`#modal-edit-value .modal-footer .modal-button-action`).click(() => {
          this.socket.get(
            `/septem/${this.data.actionLink}`,
            {
              character: window.pageCharacter,
              token: window.pagePassword
            },
            data => {
              if (!data || !data.success) {
                $(document).Toasts("create", {
                  class: "bg-danger",
                  title: `FAILED`,
                  body: `${data.error || "Unknown / Internal Error"}`,
                  autohide: true,
                  delay: 20000
                });
              }
            }
          );
        });
      });
    } else {
      $(`#modal-edit-value .modal-footer`).html("");
    }
    $("#modal-edit-value").modal("show");
  }
}
