// This file is used for managing a character on the character sheet page.

class SeptemCharacter {
  /**
   * Construct the class
   *
   * @param {string} dom The DOM query string of the container this character should go.
   * @param {object} data Initial data for the character
   * @param {sails.io} socket sails.io socket to make requests to the API
   */
  constructor(socket) {
    this.data = {}; // Used for storing a current record of the character data.
    this.socket = socket;
  }

  /**
   * Update character data
   *
   * @param {object} data New character data
   * @param {?boolean} showToasts Show toasts for changes in stats? (Default = true)
   */
  update(data, showToasts = true) {
    console.log(`UPDATE called`);
    console.dir(data);
    let changes = []; // Keep track of everything that changed

    // First, update what we do know
    for (let key in data) {
      // Calculate changes
      if (
        [
          "strength",
          "dexterity",
          "constitution",
          "intelligence",
          "wisdom",
          "charisma",
          "armor",
          "movement",
          "maxHP",
          "maxEP"
        ].indexOf(key) !== -1
      ) {
        if (
          typeof this.data[key] === "undefined" ||
          this.data[key] === null ||
          (typeof data[key] !== "undefined" &&
            data[key] !== null &&
            !shallowEqual(data[key], this.data[key]))
        )
          changes.push(
            `<strong>${key}</strong> changed to ${data[key].current}${
              typeof data[key].mod !== "undefined" ? ` (${data[key].mod})` : ``
            }`
          );
      } else {
        if (["buffs", "createdAt", "updatedAt", "items"].indexOf(key) === -1) {
          if (data[key] !== this.data[key])
            changes.push(`<strong>${key}</strong> changed to ${data[key]}.`);
        }
      }

      // Active status
      if (key === "active") {
        $(`#septem-character-card`).removeClass("card-secondary");
        $(`#septem-character-card`).removeClass("card-success");
        $(`#septem-character-card`).addClass(
          data[key] ? "card-success" : "card-secondary"
        );
        continue;
      }

      // Images
      if (key === "image") {
        if (data[key] !== null) {
          $(`#septem-character-${key}`).css(
            "background-image",
            `url("${data[key]}")`
          );
        } else {
          $(`#septem-character-${key}`).css(
            "background-image",
            `url("/images/septem/default.jpg")`
          );
        }
        continue;
      }

      // Tag
      if (key === "tag") {
        switch (data[key]) {
          case "Mini Boss":
            $(`#septem-character-tag`).html(`<div class="badge badge-warning">
            MINI BOSS
          </div>`);
            break;
          case "Final Boss":
            $(`#septem-character-tag`).html(`<div class="badge badge-danger">
            FINAL BOSS
          </div>`);
            break;
          case "Oversized Boss":
            $(`#septem-character-tag`).html(`<div class="badge bg-orange">
            OVERSIZED BOSS
          </div>`);
            break;
          case "NPC":
            $(`#septem-character-tag`).html(`<div class="badge bg-indigo">
            NPC
          </div>`);
            break;
          case "Object":
            $(`#septem-character-tag`).html(`<div class="badge badge-secondary">
            OBJECT
          </div>`);
            break;
          case "Unknown Motive":
            $(`#septem-character-tag`).html(`<div class="badge badge-info">
            UNKNOWN MOTIVE
          </div>`);
            break;
          case "Clone":
            $(`#septem-character-tag`).html(`<div class="badge bg-teal">
            CLONE
          </div>`);
            break;
          default:
            $(`#septem-character-tag`).html(`None`);
        }
        continue;
      }

      // Stats
      if (
        [
          "strength",
          "dexterity",
          "constitution",
          "intelligence",
          "wisdom",
          "charisma",
          "armor",
          "movement"
        ].indexOf(key) !== -1
      ) {
        $(`#septem-character-${key}`).html(
          `${data[key].current}${
            typeof data[key].mod !== "undefined" ? ` (${data[key].mod})` : ``
          }${key === "movement" ? ` m` : ``}`
        );
        continue;
      }

      // HP
      if (["earthHP", "matrixHP"].indexOf(key) !== -1) {
        $(`#septem-character-${key}`).html(
          `${data[key] || "???"} / ${
            typeof data.maxHP !== "undefined" ? data.maxHP.current : `???`
          }`
        );
        $(`#septem-character-${key}-progress`).css(
          "width",
          `${
            data.maxHP && data.maxHP.current
              ? (data[key] / data.maxHP.current) * 100
              : 0
          }%`
        );
        continue;
      }

      // EP
      if (["earthEP", "matrixEP"].indexOf(key) !== -1) {
        $(`#septem-character-${key}`).html(
          `${data[key] || `???`} / ${
            typeof data.maxEP !== "undefined" ? data.maxEP.current : `???`
          }`
        );
        $(`#septem-character-${key}-progress`).css(
          "width",
          `${
            data.maxEP && data.maxEP.current
              ? (data[key] / data.maxEP.current) * 100
              : 0
          }%`
        );
        continue;
      }

      // Max HP
      if (key === "maxHP") {
        ["earthHP", "matrixHP"].forEach(_key => {
          $(`#septem-character-${_key}`).html(
            `${data[_key] || `???`} / ${
              typeof data.maxHP !== "undefined" ? data.maxHP.current : `???`
            }`
          );
          $(`#septem-character-${_key}-progress`).css(
            "width",
            `${
              data.maxHP && data.maxHP.current
                ? (data[_key] / data.maxHP.current) * 100
                : 0
            }%`
          );
        });
        continue;
      }

      // Max EP
      if (key === "maxEP") {
        ["earthEP", "matrixEP"].forEach(_key => {
          $(`#septem-character-${_key}`).html(
            `${data[_key] || `???`} / ${
              typeof data.maxEP !== "undefined" ? data.maxEP.current : `???`
            }`
          );
          $(`#septem-character-${_key}-progress`).css(
            "width",
            `${
              data.maxEP && data.maxEP.current
                ? (data[_key] / data.maxEP.current) * 100
                : 0
            }%`
          );
        });
        continue;
      }

      // Percents
      if (key === "matrixHPPercent") {
        $(`#septem-character-matrixHP`).html(`${data[key]}%`);
        $(`#septem-character-matrixHP-progress`).css("width", `${data[key]}%`);
        continue;
      }
      if (key === "matrixEPPercent") {
        $(`#septem-character-matrixEP`).html(`${data[key]}%`);
        $(`#septem-character-matrixEP-progress`).css("width", `${data[key]}%`);
        continue;
      }

      // SRD
      if (key === "SRD") {
        $(`#septem-character-${key}`).html(`$${data[key].toFixed(2)}`);
        continue;
      }

      // Height
      if (key === "height") {
        $(`#septem-character-${key}`).html(
          !data[key] || data[key] === "" ? `(Not Set)` : `${data[key]} cm`
        );
        continue;
      }

      // weight
      if (key === "weight") {
        $(`#septem-character-${key}`).html(
          !data[key] || data[key] === "" ? `(Not Set)` : `${data[key]} kg`
        );
        continue;
      }

      // Standard for everything else
      $(`#septem-character-${key}`).html(
        !data[key] || data[key] === "" ? `(Not Set)` : data[key]
      );
    }

    // Set as new data
    this.data = data;

    // Update click events for inline editing on next frame
    window.requestAnimationFrame(() => {
      if (window.pageView === "gm") {
        $(`#septem-character .septem-edit`).unbind("click");
        $(`#septem-character .septem-edit`).click(e => {
          let keyClicked = $(e.currentTarget).data("key");
          let defaultValue = this.data[keyClicked];

          this.editValue(keyClicked, defaultValue);
        });
      }
    });

    // Show toasts if applicable
    if (showToasts && changes.length > 0) {
      $(document).Toasts("create", {
        class: "bg-warning",
        title: `${this.data.name || `(Redacted Name)`} (${
          this.data.id
        }) Changed`,
        body: `<ul>${changes
          .map(change => `<li>${change}</li>`)
          .join("")}</ul>`,
        autohide: true,
        delay: 20000
      });
    }
  }

  /**
   * Open a modal with an Alpaca form to edit a value with the character.
   *
   * @param {string} key Name of key to edit
   * @param {string} placeholder Default value
   */
  editValue(key, placeholder) {
    // Set null placeholders to empty strings; they work better with Alpaca
    if (placeholder === null) placeholder = "";

    // If the placeholder is an object, it came from a stat; use its base property value as the placeholder
    if (typeof placeholder === "object") {
      if (typeof placeholder.base !== "undefined")
        placeholder = placeholder.base;
    }

    // Map keys to field types
    let typeMap = {
      active: "boolean",
      isPrivate: "boolean",
      name: "string",
      image: "string",
      hasinspiration: "boolean",
      class: "string",
      age: "string",
      gender: "string",
      height: "number",
      weight: "number",
      strength: "number",
      dexterity: "number",
      constitution: "number",
      intelligence: "number",
      wisdom: "number",
      charisma: "number",
      armor: "number",
      movement: "number",
      earthHP: "number",
      earthEP: "number",
      matrixHP: "number",
      matrixEP: "number",
      maxHP: "number",
      maxEP: "number",
      XP: "number",
      SRD: "number",
      SP: "number",
      personality: "tinymce",
      appearance: "tinymce",
      biography: "tinymce",
      strengths: "tinymce",
      weaknesses: "tinymce",
      gmNotes: "tinymce"
    };

    // Determine the field type for this key
    let theType = typeMap[key];

    // Pre-determine our field
    let properties = {
      value: {
        title: `${key} for ${this.data.name} (${this.data.id})`,
        type: theType,
        required: theType === "number"
      }
    };

    // Pre-determine our options
    let options = {
      fields: {},
      form: {
        buttons: {
          submit: {
            title: `Edit Value`,
            click: (form, e) => {
              form.refreshValidationState(true);
              if (!form.isValid(true)) {
                form.focus();
                return;
              }
              let value = form.getValue();

              let update = {};
              update[key] = value.value;
              update.token = window.pagePassword;

              // If adjust was set, adjust the new value by the value of adjust
              if (typeof value.adjust !== "undefined")
                update[key] += value.adjust;

              this.socket.patch(
                `/septemcharacters/${this.data.id}`,
                update,
                resData => {
                  $("#modal-edit-value").modal("hide");
                }
              );
            }
          }
        }
      }
    };

    // If field type is tinymce, set tinymce options
    if (theType === "tinymce") {
      theType = "string";
      options.fields.value = {
        type: "tinymce",
        options: {
          toolbar:
            "undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | fullscreen preview | image link | ltr rtl",
          plugins:
            "autoresize preview paste importcss searchreplace autolink save directionality visualblocks visualchars fullscreen image link table hr pagebreak nonbreaking toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help quickbars",
          menubar: "file edit view insert format tools table help"
        }
      };
    }

    // If field type is a number, add adjust field
    if (theType === "number") {
      properties.adjust = {
        title: `-OR- Adjust ${key} by`,
        type: "number",
        default: 0
      };
    }

    // Create form
    $(`#modal-edit-value .modal-body`).html("");
    $(`#modal-edit-value .modal-body`).alpaca({
      schema: {
        type: "object",
        properties: properties
      },
      options: options,
      data: { value: placeholder }
    });

    $(`#modal-edit-value .modal-footer`).html("");

    $("#modal-edit-value").modal("show");
  }
}

// Helper
function shallowEqual(object1, object2) {
  const keys1 = Object.keys(object1);
  const keys2 = Object.keys(object2);

  if (keys1.length !== keys2.length) {
    return false;
  }

  for (let key of keys1) {
    if (object1[key] !== object2[key]) {
      return false;
    }
  }

  return true;
}
