// This manages character buffs on the character sheet

class SeptemCharacterBuffs {
  /**
   * Construct the class
   *
   * @param {string} buffs DOM query string of the container where buffs should be listed
   * @param {string} modal DOM query string of the modal to use when clicking a buff for more information
   * @param {sails.io} socket sails.io socket to make requests to the API
   */
  constructor(buffs, modal, socket) {
    // Store the DOM strings
    this.dom = {
      buffs: buffs,
      modal: modal
    };

    // Construct a map for our buffs
    this.buffs = new Map();

    this.socket = socket;
  }

  /**
   * Process data from septem/get/character
   *
   * @param {Array} data Result of character.buffs
   */
  process(data) {
    // Remember which buffs currently exist so we can remove old ones.
    let existing = [];

    // Process existing buffs
    if (data.length > 0) {
      data.forEach(buff => {
        existing.push(buff.id);

        // new buff? Create a record for it
        if (!this.buffs.has(buff.id)) {
          console.log(`New buff: ${buff.id}`);
          this.add(buff);
          return;
        }

        let prevBuff = this.buffs.get(buff.id);

        // Beyond this point, update buff with new data
        if (prevBuff) {
          prevBuff.update(buff);
        }
      });
    }

    // Remove buffs that no longer exist
    let memoryBuffs = Array.from(this.buffs.keys());
    console.dir(memoryBuffs);
    console.dir(existing);
    if (memoryBuffs.length > 0) {
      memoryBuffs.forEach(buf => {
        if (existing.indexOf(buf) === -1) {
          console.log(`Did not find buff ${buf} anymore.`);
          this.remove(buf);
        }
      });
    }
  }

  /**
   * Create a new buff
   *
   * @param {object} buff buff data
   */
  add(buff) {
    this.buffs.set(
      buff.id,
      new SeptemCharacterBuff(this.dom.buffs, buff, socket)
    );
  }

  /**
   * Remove a buff.
   *
   * @param {number} id ID of the buff to remove
   */
  remove(id) {
    if (this.buffs.has(id)) {
      console.log(`Removing buff ${id}`);
      let buff = this.buffs.get(id);
      buff.remove(); // Remove the card
      this.buffs.delete(id);
    }
  }

  /**
   * Get a buff.
   *
   * @param {number} id ID of the buff to fetch
   */
  get(id) {
    return this.buffs.get(id);
  }
}

// Manager for a single buff. Do NOT use this directly; use SeptemCharacterBuffs instead.
class SeptemCharacterBuff {
  /**
   * Construct the class
   *
   * @param {string} dom The DOM query string of the container this buff should go.
   * @param {object} data Initial data for the buff
   * @param {sails.io} socket sails.io socket to make requests to the API
   */
  constructor(dom, data, socket) {
    this.dom = dom;
    this.data = data; // Used for storing a current record of the buff data.
    this.socket = socket;

    // Make the record
    this.makeRecord(this.dom);

    // On next frame, update record with data
    window.requestAnimationFrame(() => {
      this.update(this.data, false);
    });
  }

  /**
   * Construct the buff record.
   *
   * @param {string} dom The DOM to append the record in.
   */
  makeRecord(dom) {
    $(dom).append(
      `<p id="septem-characterbuff-${this.data.id}"><strong id="septem-characterbuff-${this.data.id}-name"></strong> (<span id="septem-characterbuff-${this.data.id}-duration"></span>)</p>`
    );
  }

  /**
   * Call this before removing the character from the manager so that the card is removed.
   */
  remove() {
    $(`#septem-characterbuff-${this.data.id}`).remove();
  }

  /**
   * Update character data
   *
   * @param {object} data New character data
   * @param {?boolean} showToasts Show toasts for changes in stats? (Default = true)
   */
  update(data, showToasts = true) {
    let changes = []; // Keep track of everything that changed

    // First, update what we do know
    for (let key in data) {
      // Editing inline
      if (window.pageView === "gm") {
        $(`#septem-characterbuff-${this.data.id} .septem-edit`).unbind("click");
        $(`#septem-characterbuff-${this.data.id} .septem-edit`).click(e => {
          let keyClicked = $(e.currentTarget).data("key");
          let defaultValue = this.data[keyClicked];

          this.editValue(keyClicked, defaultValue);
        });
      }

      if (key === "duration") {
        let toDisplay = `Untimed`;
        if (data[key] !== null) {
          if (data[key] >= 60 * 24) {
            toDisplay = `${Math.floor(data[key] / (60 * 24))} days remaining`;
          } else if (data[key] >= 60) {
            toDisplay = `${Math.floor(data[key] / 60)} hours remaining`;
          } else {
            toDisplay = `${data[key]} minutes remaining`;
          }
        }
        $(`#septem-characterbuff-${this.data.id}-${key}`).html(toDisplay);
        continue;
      }

      // Standard for everything else
      $(`#septem-characterbuff-${this.data.id}-${key}`).html(
        !data[key] || data[key] === "" ? `(Not Set)` : data[key]
      );
    }

    // Set as new data
    this.data = data;

    // Show toasts if applicable
    if (showToasts && changes.length > 0) {
      $(document).Toasts("create", {
        class: "bg-warning",
        title: `${this.data.name || `(Redacted Name)`} (${
          this.data.id
        }) Changed`,
        body: `<ul>${changes
          .map(change => `<li>${change}</li>`)
          .join("")}</ul>`,
        autohide: true,
        delay: 20000
      });
    }
  }

  /**
   * Open a modal with an Alpaca form to edit a value with the character.
   *
   * @param {string} key Name of key to edit
   * @param {string} placeholder Default value
   */
  editValue(key, placeholder) {
      // TODO
      /*
    if (placeholder === null) placeholder = "";
    if (typeof placeholder === "object") {
      if (typeof placeholder.base !== "undefined")
        placeholder = placeholder.base;
    }

    let typeMap = {
      active: "boolean",
      isPrivate: "boolean",
      name: "string",
      image: "string",
      hasinspiration: "boolean",
      class: "string",
      height: "number",
      weight: "number",
      strength: "number",
      dexterity: "number",
      constitution: "number",
      intelligence: "number",
      wisdom: "number",
      charisma: "number",
      armor: "number",
      movement: "number",
      earthHP: "number",
      earthEP: "number",
      matrixHP: "number",
      matrixEP: "number",
      maxHP: "number",
      maxEP: "number",
      XP: "number",
      SRD: "number",
      SP: "number"
    };

    // Create form
    $(`#modal-edit-value .modal-body`).html("");
    $(`#modal-edit-value .modal-body`).alpaca({
      schema: {
        type: "object",
        properties: {
          value: {
            title: `${key} for ${this.data.name} (${this.data.id})`,
            type: typeMap[key],
            required: typeMap[key] === "number"
          }
        }
      },
      options: {
        form: {
          buttons: {
            submit: {
              title: `Edit Value`,
              click: (form, e) => {
                form.refreshValidationState(true);
                if (!form.isValid(true)) {
                  form.focus();
                  return;
                }
                let value = form.getValue();

                let update = {};
                update[key] = value.value;
                update.token = window.pagePassword;

                this.socket.patch(
                  `/septemcharacters/${this.data.id}`,
                  update,
                  resData => {
                    $("#modal-edit-value").modal("hide");
                  }
                );
              }
            }
          }
        }
      },
      data: { value: placeholder }
    });

    $("#modal-edit-value").modal("show");
    */
  }
}
