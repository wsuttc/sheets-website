/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  /***************************************************************************
   *                                                                          *
   * Make the view located at `views/homepage.ejs` your home page.            *
   *                                                                          *
   * (Alternatively, remove this and add an `index.html` file in your         *
   * `assets` directory)                                                      *
   *                                                                          *
   ***************************************************************************/

  "GET /csrfToken": { action: "security/grant-csrf-token" },

  /***************************************************************************
   *                                                                          *
   * More custom routes here...                                               *
   * (See https://sailsjs.com/config/routes for examples.)                    *
   *                                                                          *
   * If a request to a URL doesn't match any of the routes in this file, it   *
   * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
   * not match any of those, it is matched against static assets.             *
   *                                                                          *
   ***************************************************************************/

  "GET /": {
    view: "home",
    locals: {}
  },

  // NGINX sometimes adds index.html directive. We want this to also direct to the homepage.
  "GET /index.html": {
    view: "home",
    locals: {}
  },

  /*
      SEPTEM REGIONES
  */

  // About page
  "GET /septem": {
    view: "septem/about",
    locals: {}
  },

  // Player panel
  "GET /septem/player": {
    view: "septem/playerpanel",
    locals: {
      pageView: "player"
    }
  },

  // Restricted panels where no password was provided
  "GET /septem/operator": {
    view: "septem/restricted/panel",
    locals: {}
  },
  "GET /septem/scanner": {
    view: "septem/restricted/panel",
    locals: {}
  },
  "GET /septem/gm": {
    view: "septem/restricted/panel",
    locals: {}
  },

  // Panels with password provided
  "GET /septem/gm/:password": function(req, res) {
    let pagePassword = req.param("password");
    return res.view("septem/gmpanel", {
      pageView: "gm",
      pagePassword: pagePassword
    });
  },
  "GET /septem/operator/:password": function(req, res) {
    let pagePassword = req.param("password");
    return res.view("septem/operatorpanel", {
      pageView: "operator",
      pagePassword: pagePassword
    });
  },
  "GET /septem/scanner/:password": function(req, res) {
    let pagePassword = req.param("password");
    return res.view("septem/scannerpanel", {
      pageView: "scanner",
      pagePassword: pagePassword
    });
  },

  // Character sheets
  "GET /septem/player/character/:character": function(req, res) {
    let character = req.param("character");
    return res.view("septem/charactersheet", {
      pageView: "player",
      pagePassword: "",
      pageCharacter: character
    });
  },
  "GET /septem/:view/:password/character/:character": function(req, res) {
    let view = req.param("view");
    let password = req.param("password");
    let character = req.param("character");
    return res.view("septem/charactersheet", {
      pageView: view,
      pagePassword: password,
      pageCharacter: character
    });
  }
};
