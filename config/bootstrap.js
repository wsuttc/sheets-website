/**
 * Seed Function
 * (sails.config.bootstrap)
 *
 * A function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also create a hook.
 *
 * For more information on seeding your app with fake data, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function() {
  // Create septem global record if it does not exist
  await sails.models.septemglobals.findOrCreate({ id: 1 }, { id: 1 });

  // Create passwords
  await sails.models.septempasswords.findOrCreate(
    { name: "operator" },
    { name: "operator", value: await sails.helpers.uid() }
  );
  await sails.models.septempasswords.findOrCreate(
    { name: "gm" },
    { name: "gm", value: await sails.helpers.uid() }
  );
  await sails.models.septempasswords.findOrCreate(
    { name: "scanner" },
    { name: "scanner", value: await sails.helpers.uid() }
  );
};
